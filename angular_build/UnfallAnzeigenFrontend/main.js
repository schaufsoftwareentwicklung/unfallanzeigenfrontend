(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "+QUk":
/*!***************************************************************************************************!*\
  !*** ./src/app/shared-modules/display/days-without-accidents/days-without-accidents.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: DaysWithoutAccidentsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DaysWithoutAccidentsComponent", function() { return DaysWithoutAccidentsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/form-field */ "kmnG");
/* harmony import */ var _directives_number_only_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../directives/number-only.directive */ "ggr/");




class DaysWithoutAccidentsComponent {
    constructor() {
        this.accidents = 0;
        this.accidentsChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.manuelModus = true;
    }
    ngOnInit() {
    }
    updateAccidents(event) {
        if (event !== undefined && event !== null) {
            if (event.target !== undefined && event.target !== null) {
                if (event.target.value !== undefined || event.target.value !== null) {
                    if (event.target.value === "") {
                        this.accidents = 0;
                        this.accidentsChange.emit(0);
                    }
                    else {
                        this.accidents = Number(event.target.value);
                        this.accidentsChange.emit(this.accidents);
                    }
                }
            }
        }
    }
}
DaysWithoutAccidentsComponent.ɵfac = function DaysWithoutAccidentsComponent_Factory(t) { return new (t || DaysWithoutAccidentsComponent)(); };
DaysWithoutAccidentsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: DaysWithoutAccidentsComponent, selectors: [["app-days-without-accidents"]], inputs: { accidents: "accidents", manuelModus: "manuelModus" }, outputs: { accidentsChange: "accidentsChange" }, decls: 4, vars: 2, consts: [[1, "row"], [1, "col-8", "displayText", "displayLabel"], ["MatInput", "", "appNumberOnly", "", "maxlength", "4", 1, "col-4", "displayInput", "accident", 3, "value", "disabled", "change"]], template: function DaysWithoutAccidentsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-label", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Tage ohne Unf\u00E4lle:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "input", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function DaysWithoutAccidentsComponent_Template_input_change_3_listener($event) { return ctx.updateAccidents($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.accidents)("disabled", !ctx.manuelModus);
    } }, directives: [_angular_material_form_field__WEBPACK_IMPORTED_MODULE_1__["MatLabel"], _directives_number_only_directive__WEBPACK_IMPORTED_MODULE_2__["NumberOnlyDirective"]], styles: [".row[_ngcontent-%COMP%] {\n  margin: 2px;\n}\n\n.display[_ngcontent-%COMP%] {\n  background-color: black;\n  border-color: #a7a7a7;\n  border-width: 5px;\n  border-style: solid;\n  height: auto;\n  min-height: 50px;\n  padding: 3px;\n}\n\n.displayText[_ngcontent-%COMP%] {\n  background-color: black;\n  color: white;\n  align-items: center;\n}\n\n.displayHeader[_ngcontent-%COMP%] {\n  text-decoration: underline;\n  text-align: center;\n  font-size: 35px;\n  height: 60px;\n  width: 99%;\n  display: grid;\n}\n\n.displayLabel[_ngcontent-%COMP%] {\n  font-size: 30px;\n  display: flex;\n}\n\n.displayInput[_ngcontent-%COMP%] {\n  background-color: black;\n  border-color: #a7a7a7;\n  border-width: 2px;\n  border-style: solid;\n  color: red;\n  padding: 4px;\n  height: 50;\n  font-size: 40px;\n  -webkit-appearance: none;\n  -moz-appearance: none;\n}\n\n.accident[_ngcontent-%COMP%] {\n  text-align: right;\n  font-family: \"numeric\";\n}\n\n.notification[_ngcontent-%COMP%] {\n  font-family: \"editundot\";\n  text-align: center;\n  margin: 2px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxkaXNwbGF5LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksMEJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7QUFDSjs7QUFHQTtFQUNJLGVBQUE7RUFDQSxhQUFBO0FBQUo7O0FBR0E7RUFDSSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFFQSx3QkFBQTtFQUNBLHFCQUFBO0FBREo7O0FBSUE7RUFDSSxpQkFBQTtFQUNBLHNCQUFBO0FBREo7O0FBSUE7RUFDSSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQURKIiwiZmlsZSI6ImRpc3BsYXkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucm93IHtcclxuICAgIG1hcmdpbjogMnB4O1xyXG59XHJcblxyXG4uZGlzcGxheSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICAgIGJvcmRlci1jb2xvcjogcmdiKDE2NywgMTY3LCAxNjcpO1xyXG4gICAgYm9yZGVyLXdpZHRoOiA1cHg7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgbWluLWhlaWdodDogNTBweDtcclxuICAgIHBhZGRpbmc6IDNweDtcclxufVxyXG5cclxuLmRpc3BsYXlUZXh0e1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4uZGlzcGxheUhlYWRlcntcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAzNXB4O1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgd2lkdGg6IDk5JTtcclxuICAgIGRpc3BsYXk6IGdyaWQ7XHJcbn1cclxuXHJcblxyXG4uZGlzcGxheUxhYmVse1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuLmRpc3BsYXlJbnB1dHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMTY3LCAxNjcsIDE2Nyk7XHJcbiAgICBib3JkZXItd2lkdGg6IDJweDtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgcGFkZGluZzogNHB4O1xyXG4gICAgaGVpZ2h0OiA1MDtcclxuICAgIGZvbnQtc2l6ZTogNDBweDtcclxuXHJcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6bm9uZTsgXHJcbiAgICAtbW96LWFwcGVhcmFuY2U6IG5vbmU7IFxyXG59XHJcblxyXG4uYWNjaWRlbnQge1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBmb250LWZhbWlseTogJ251bWVyaWMnO1xyXG59XHJcblxyXG4ubm90aWZpY2F0aW9uIHtcclxuICAgIGZvbnQtZmFtaWx5OiAnZWRpdHVuZG90JztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMnB4O1xyXG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DaysWithoutAccidentsComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-days-without-accidents',
                templateUrl: './days-without-accidents.component.html',
                styleUrls: [
                    '../display.component.scss'
                ]
            }]
    }], function () { return []; }, { accidents: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], accidentsChange: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }], manuelModus: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }] }); })();


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Projekts\UnfallAnzeigen\UnfallAnzeigenFrontend\src\main.ts */"zUnb");


/***/ }),

/***/ "4wHx":
/*!**************************************************************!*\
  !*** ./src/app/layout-designer/layout-designer.component.ts ***!
  \**************************************************************/
/*! exports provided: LayoutDesignerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutDesignerComponent", function() { return LayoutDesignerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _Models_Display_Model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Models/Display.Model */ "QX5R");
/* harmony import */ var _Services_working_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Services/working.service */ "VcEb");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/list */ "MutI");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _shared_modules_display_display_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared-modules/display/display.component */ "zzkD");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/form-field */ "kmnG");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "5+WD");









function LayoutDesignerComponent_mat_list_option_8_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-list-option", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function LayoutDesignerComponent_mat_list_option_8_Template_mat_list_option_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const i_r2 = ctx.index; const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.onSelectDisplay(i_r2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const display_r1 = ctx.$implicit;
    const i_r2 = ctx.index;
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("selected", i_r2 == ctx_r0.selectedDisplayIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](display_r1.name);
} }
class LayoutDesignerComponent {
    constructor(workingService) {
        this.workingService = workingService;
        this.selectedDisplay = new _Models_Display_Model__WEBPACK_IMPORTED_MODULE_1__["IDisplay"]();
        this.subscriptions = [];
        this.displays = [];
        this.selectedDisplayIndex = -1;
        this.selectedInterfaceIndex = -1;
        this.logText = "";
        this.isTimeValid = false;
        this.isDateValid = false;
    }
    ngOnInit() {
        this.workingService.initialize();
        setTimeout(() => {
            this.subscriptions.push(this.workingService.displaysChanged.subscribe(displays => {
                this.displays = [...displays];
                if (this.selectedDisplayIndex !== -1) {
                    this.selectedDisplay = this.displays[this.selectedDisplayIndex];
                }
            }));
            this.subscriptions.push(this.workingService.LogSubject.subscribe(log => {
                this.logText += log;
            }));
            setTimeout(() => {
                this.workingService.triggerDisplaysChanged();
                setTimeout(() => {
                    if (this.displays.length != 0) {
                        {
                            this.onSelectDisplay(0);
                            if (this.displays[this.selectedDisplayIndex].interfaces.length != 0) {
                                this.onSelectInterface(0);
                            }
                        }
                    }
                }, 500);
            }, 500);
        }, 500);
    }
    onSelectDisplay(index) {
        this.workingService.selectedDisplaySubject.next(index);
        this.selectedDisplay = this.displays[index];
        this.selectedDisplayIndex = index;
    }
    onSelectInterface(index) {
        this.selectedInterfaceIndex = index;
        if (this.selectedDisplay.useAccidentsLastYear || this.selectedDisplay.useAccidentsThisYear ||
            this.selectedDisplay.useDaysWithoutAccidents || this.selectedDisplay.useDaysWithoutAccidentsHighscore) {
            this.workingService.interfaceSelected(this.selectedInterfaceIndex);
        }
    }
    ngOnDestroy() {
        this.subscriptions.forEach(sub => {
            sub.unsubscribe();
        });
    }
    onDisplayChanged(event) {
        this.selectedDisplay = event;
    }
    setTime(event) {
        if (event !== undefined && event !== null) {
            if (event.target !== undefined && event.target !== null) {
                if (event.target.value !== undefined || event.target.value !== null) {
                    if (event.target.value.match(/^([0-2][0-3]:[0-5][0-9]|[0-1][0-9]:[0-5][0-9])$/g)) {
                        this.isTimeValid = true;
                        this.selectedDisplay.time = event.target.value;
                    }
                    else {
                        this.isTimeValid = false;
                    }
                }
            }
        }
    }
    reportAccident() {
        this.workingService.reportAccident(this.selectedDisplay);
    }
    sendData() {
        this.workingService.sendAccidentData(this.selectedDisplay);
    }
    saveTime() {
        this.workingService.sendDateTimeData(this.selectedDisplay);
    }
    clearLog() {
        this.logText = "";
    }
}
LayoutDesignerComponent.ɵfac = function LayoutDesignerComponent_Factory(t) { return new (t || LayoutDesignerComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_Services_working_service__WEBPACK_IMPORTED_MODULE_2__["WorkingService"])); };
LayoutDesignerComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: LayoutDesignerComponent, selectors: [["app-layout-designer"]], decls: 25, vars: 3, consts: [[1, "row"], [1, "col-3"], [1, "outlineBorder"], [1, "header"], [1, "group"], [3, "multiple"], [3, "selected", "click", 4, "ngFor", "ngForOf"], [1, "col-6"], [1, "col-12"], [3, "display", "displayChanged"], [1, "group", "outlineBorder"], [1, "subsectionHeader"], ["value", "asdfl\u00F6kjasdf", "cdkDrag", ""], ["cdkDrag", ""], [3, "selected", "click"]], template: function LayoutDesignerComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Anzeigen:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-selection-list", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, LayoutDesignerComponent_mat_list_option_8_Template, 2, 2, "mat-list-option", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "app-display", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("displayChanged", function LayoutDesignerComponent_Template_app_display_displayChanged_11_listener($event) { return ctx.onDisplayChanged($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Einstellungen:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "mat-label", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Elemente:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "input", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, " asdf");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "mat-label", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Eigenschaften:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("multiple", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.displays);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("display", ctx.selectedDisplay);
    } }, directives: [_angular_material_list__WEBPACK_IMPORTED_MODULE_3__["MatSelectionList"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"], _shared_modules_display_display_component__WEBPACK_IMPORTED_MODULE_5__["DisplayComponent"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_6__["MatLabel"], _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_7__["CdkDrag"], _angular_material_list__WEBPACK_IMPORTED_MODULE_3__["MatListOption"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsYXlvdXQtZGVzaWduZXIuY29tcG9uZW50LnNjc3MifQ== */", "mat-selection-list[_ngcontent-%COMP%] {\n  max-height: 80vh;\n  overflow: auto;\n}\n\nmat-list-option[_ngcontent-%COMP%] {\n  border: solid 1px #CED4DA;\n}\n\nmat-list-option[_ngcontent-%COMP%]:hover {\n  background-color: #8898f7;\n}\n\n.mat-list-item.mat-list-single-selected-option[_ngcontent-%COMP%] {\n  background: #3F51B5;\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxsaXN0LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLHlCQUFBO0FBQ0o7O0FBR0E7RUFDSSx5QkFBQTtBQUFKOztBQUdBO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0FBQUoiLCJmaWxlIjoibGlzdC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibWF0LXNlbGVjdGlvbi1saXN0e1xyXG4gICAgbWF4LWhlaWdodDogODB2aDtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gIH1cclxuXHJcbm1hdC1saXN0LW9wdGlvbiB7XHJcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjQ0VENERBO1xyXG4gICAgXHJcbn1cclxuXHJcbm1hdC1saXN0LW9wdGlvbjpob3ZlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM4ODk4Zjc7XHJcbn1cclxuXHJcbi5tYXQtbGlzdC1pdGVtLm1hdC1saXN0LXNpbmdsZS1zZWxlY3RlZC1vcHRpb24ge1xyXG4gICAgYmFja2dyb3VuZDogIzNGNTFCNTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcblxyXG5cclxuIl19 */", ".col[_ngcontent-%COMP%] {\n  height: 100hv;\n}\n\n.row[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.subsectionHeader[_ngcontent-%COMP%] {\n  font-size: 19px;\n}\n\n.group[_ngcontent-%COMP%] {\n  margin-bottom: 7px;\n}\n\n.header[_ngcontent-%COMP%] {\n  text-decoration: underline;\n  text-align: center;\n  font-size: 24px;\n}\n\n.center[_ngcontent-%COMP%] {\n  width: 100%;\n  display: block;\n  text-align: center;\n}\n\nbutton[_ngcontent-%COMP%] {\n  margin-right: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxsYXlvdXQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7QUFDSiIsImZpbGUiOiJsYXlvdXQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb2wge1xyXG4gICAgaGVpZ2h0OiAxMDBodjtcclxufVxyXG5cclxuLnJvd3tcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uc3Vic2VjdGlvbkhlYWRlcntcclxuICAgIGZvbnQtc2l6ZTogMTlweCA7XHJcbn1cclxuXHJcbi5ncm91cHtcclxuICAgIG1hcmdpbi1ib3R0b206IDdweDtcclxufVxyXG5cclxuLmhlYWRlcntcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAyNHB4O1xyXG59XHJcblxyXG4uY2VudGVye1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuYnV0dG9ue1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn0iXX0= */", ".outlineBorder[_ngcontent-%COMP%] {\n  margin: 2px;\n  border: solid 1px #CED4DA;\n  padding: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxib3guc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7QUFDSiIsImZpbGUiOiJib3guc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5vdXRsaW5lQm9yZGVye1xyXG4gICAgbWFyZ2luOiAycHg7XHJcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjQ0VENERBO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG59XHJcbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LayoutDesignerComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-layout-designer',
                templateUrl: './layout-designer.component.html',
                styleUrls: [
                    './layout-designer.component.scss',
                    '../styles/global/list.scss',
                    '../styles/global/layout.scss',
                    '../styles/global/box.scss'
                ]
            }]
    }], function () { return [{ type: _Services_working_service__WEBPACK_IMPORTED_MODULE_2__["WorkingService"] }]; }, null); })();


/***/ }),

/***/ "89mU":
/*!************************************************!*\
  !*** ./src/app/Services/app-config.service.ts ***!
  \************************************************/
/*! exports provided: AppConfigService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppConfigService", function() { return AppConfigService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");



class AppConfigService {
    constructor(http) {
        this.http = http;
    }
    loadAppConfig() {
        return this.http.get('assets/appConfig.json')
            .toPromise()
            .then(config => {
            this.appConfig = config;
        });
    }
    get webAPIServer() {
        if (this.appConfig.WebAPIServer == "local") {
            console.log('--------------------------------------------------------');
            console.log('--------------------------------------------------------');
        }
        return this.appConfig.WebAPIServer;
    }
}
AppConfigService.ɵfac = function AppConfigService_Factory(t) { return new (t || AppConfigService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
AppConfigService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: AppConfigService, factory: AppConfigService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppConfigService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "Aq2v":
/*!*****************************************************************!*\
  !*** ./src/app/configuration/configuration-facade.component.ts ***!
  \*****************************************************************/
/*! exports provided: ConfigurationFacadeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigurationFacadeComponent", function() { return ConfigurationFacadeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_Models_Display_Model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/Models/Display.Model */ "QX5R");
/* harmony import */ var src_app_Services_configuration_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Services/configuration.service */ "JYHc");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/button */ "bTqV");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/icon */ "NFeN");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/list */ "MutI");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _display_configuration_form_display_configuration_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./display-configuration-form/display-configuration-form.component */ "DrwV");
/* harmony import */ var _shared_modules_display_display_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared-modules/display/display.component */ "zzkD");










function ConfigurationFacadeComponent_mat_list_option_17_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-list-option", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ConfigurationFacadeComponent_mat_list_option_17_Template_mat_list_option_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const i_r2 = ctx.index; const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.onEditItem(i_r2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const display_r1 = ctx.$implicit;
    const i_r2 = ctx.index;
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("selected", i_r2 == ctx_r0.selectedDisplayIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](display_r1.name);
} }
class ConfigurationFacadeComponent {
    constructor(configurationService) {
        this.configurationService = configurationService;
        this.displays = [];
        this.selectedDisplayIndex = -1;
        this.currentDisplayConfig = new src_app_Models_Display_Model__WEBPACK_IMPORTED_MODULE_1__["IDisplay"]();
    }
    ngOnInit() {
        this.configurationService.initialize();
        setTimeout(() => {
            this.subscription = this.configurationService.displaysChanged.subscribe(displays => {
                this.displays = [...displays];
            });
            setTimeout(() => {
                this.configurationService.triggerDisplaysChanged();
                setTimeout(() => {
                    if (this.displays.length == 0) {
                        this.newDisplay();
                    }
                    this.onEditItem(0);
                }, 500);
            }, 500);
        }, 500);
    }
    configChanged(displayConfig) {
        this.currentDisplayConfig = displayConfig;
    }
    newDisplay() {
        this.configurationService.newDisplay();
        // this.onEditItem(this.displays.length -1);
    }
    deleteSelectedDisplay() {
        this.configurationService.deleteDisplay(this.selectedDisplayIndex);
        this.selectedDisplayIndex = -1;
    }
    duplicateSelectedDisplay() {
        this.configurationService.duplicateDisplay(this.selectedDisplayIndex);
    }
    onEditItem(index) {
        this.configurationService.startedEditing.next(index);
        this.selectedDisplayIndex = index;
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
ConfigurationFacadeComponent.ɵfac = function ConfigurationFacadeComponent_Factory(t) { return new (t || ConfigurationFacadeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_Services_configuration_service__WEBPACK_IMPORTED_MODULE_2__["ConfigurationService"])); };
ConfigurationFacadeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ConfigurationFacadeComponent, selectors: [["app-configuration-facade"]], decls: 25, vars: 5, consts: [[1, "row"], [1, "col-3"], [1, "outlineBorder"], [1, "header"], [1, "group", "center"], ["mat-raised-button", "", "color", "primary", "type", "button", 3, "click"], ["aria-label", "Neue Anzeige"], ["mat-raised-button", "", "color", "primary", "type", "button", 3, "disabled", "click"], ["aria-label", "Anzeige l\u00F6schen"], ["aria-label", "Anzeige verdoppeln"], [3, "multiple"], [3, "selected", "click", 4, "ngFor", "ngForOf"], [3, "displayConfig"], [1, "col-6"], [2, "margin-top", "5%"], [3, "display"], [3, "selected", "click"]], template: function ConfigurationFacadeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Anzeigen:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "button", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ConfigurationFacadeComponent_Template_button_click_7_listener() { return ctx.newDisplay(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "mat-icon", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "add_circle");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ConfigurationFacadeComponent_Template_button_click_10_listener() { return ctx.deleteSelectedDisplay(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-icon", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "delete");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ConfigurationFacadeComponent_Template_button_click_13_listener() { return ctx.duplicateSelectedDisplay(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "mat-icon", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "content_copy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "mat-selection-list", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, ConfigurationFacadeComponent_mat_list_option_17_Template, 2, 2, "mat-list-option", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "app-display-configuration-form", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("displayConfig", function ConfigurationFacadeComponent_Template_app_display_configuration_form_displayConfig_19_listener($event) { return ctx.configChanged($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Layout:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "app-display", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", ctx.selectedDisplayIndex == -1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", ctx.selectedDisplayIndex == -1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("multiple", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.displays);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("display", ctx.currentDisplayConfig);
    } }, directives: [_angular_material_button__WEBPACK_IMPORTED_MODULE_3__["MatButton"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_4__["MatIcon"], _angular_material_list__WEBPACK_IMPORTED_MODULE_5__["MatSelectionList"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgForOf"], _display_configuration_form_display_configuration_form_component__WEBPACK_IMPORTED_MODULE_7__["DisplayConfigurationFormComponent"], _shared_modules_display_display_component__WEBPACK_IMPORTED_MODULE_8__["DisplayComponent"], _angular_material_list__WEBPACK_IMPORTED_MODULE_5__["MatListOption"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb25maWd1cmF0aW9uLWZhY2FkZS5jb21wb25lbnQuc2NzcyJ9 */", "mat-selection-list[_ngcontent-%COMP%] {\n  max-height: 80vh;\n  overflow: auto;\n}\n\nmat-list-option[_ngcontent-%COMP%] {\n  border: solid 1px #CED4DA;\n}\n\nmat-list-option[_ngcontent-%COMP%]:hover {\n  background-color: #8898f7;\n}\n\n.mat-list-item.mat-list-single-selected-option[_ngcontent-%COMP%] {\n  background: #3F51B5;\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxsaXN0LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLHlCQUFBO0FBQ0o7O0FBR0E7RUFDSSx5QkFBQTtBQUFKOztBQUdBO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0FBQUoiLCJmaWxlIjoibGlzdC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibWF0LXNlbGVjdGlvbi1saXN0e1xyXG4gICAgbWF4LWhlaWdodDogODB2aDtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gIH1cclxuXHJcbm1hdC1saXN0LW9wdGlvbiB7XHJcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjQ0VENERBO1xyXG4gICAgXHJcbn1cclxuXHJcbm1hdC1saXN0LW9wdGlvbjpob3ZlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM4ODk4Zjc7XHJcbn1cclxuXHJcbi5tYXQtbGlzdC1pdGVtLm1hdC1saXN0LXNpbmdsZS1zZWxlY3RlZC1vcHRpb24ge1xyXG4gICAgYmFja2dyb3VuZDogIzNGNTFCNTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcblxyXG5cclxuIl19 */", ".col[_ngcontent-%COMP%] {\n  height: 100hv;\n}\n\n.row[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.subsectionHeader[_ngcontent-%COMP%] {\n  font-size: 19px;\n}\n\n.group[_ngcontent-%COMP%] {\n  margin-bottom: 7px;\n}\n\n.header[_ngcontent-%COMP%] {\n  text-decoration: underline;\n  text-align: center;\n  font-size: 24px;\n}\n\n.center[_ngcontent-%COMP%] {\n  width: 100%;\n  display: block;\n  text-align: center;\n}\n\nbutton[_ngcontent-%COMP%] {\n  margin-right: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxsYXlvdXQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7QUFDSiIsImZpbGUiOiJsYXlvdXQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb2wge1xyXG4gICAgaGVpZ2h0OiAxMDBodjtcclxufVxyXG5cclxuLnJvd3tcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uc3Vic2VjdGlvbkhlYWRlcntcclxuICAgIGZvbnQtc2l6ZTogMTlweCA7XHJcbn1cclxuXHJcbi5ncm91cHtcclxuICAgIG1hcmdpbi1ib3R0b206IDdweDtcclxufVxyXG5cclxuLmhlYWRlcntcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAyNHB4O1xyXG59XHJcblxyXG4uY2VudGVye1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuYnV0dG9ue1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn0iXX0= */", ".outlineBorder[_ngcontent-%COMP%] {\n  margin: 2px;\n  border: solid 1px #CED4DA;\n  padding: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxib3guc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7QUFDSiIsImZpbGUiOiJib3guc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5vdXRsaW5lQm9yZGVye1xyXG4gICAgbWFyZ2luOiAycHg7XHJcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjQ0VENERBO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG59XHJcbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ConfigurationFacadeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-configuration-facade',
                templateUrl: './configuration-facade.component.html',
                styleUrls: [
                    './configuration-facade.component.scss',
                    '../styles/global/list.scss',
                    '../styles/global/layout.scss',
                    '../styles/global/box.scss'
                ]
            }]
    }], function () { return [{ type: src_app_Services_configuration_service__WEBPACK_IMPORTED_MODULE_2__["ConfigurationService"] }]; }, null); })();


/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "C98j":
/*!*******************************************!*\
  !*** ./src/app/working/working.module.ts ***!
  \*******************************************/
/*! exports provided: WorkingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WorkingModule", function() { return WorkingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/list */ "MutI");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/icon */ "NFeN");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/form-field */ "kmnG");
/* harmony import */ var _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared-modules/display/display.module */ "Z/RF");
/* harmony import */ var _working_facade_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./working-facade.component */ "xdQH");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/button */ "bTqV");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "tyNb");











const routes = [
    { path: '', redirectTo: '/Anzeigen', pathMatch: 'full' },
    { path: 'Anzeigen', component: _working_facade_component__WEBPACK_IMPORTED_MODULE_6__["WorkingFacadeComponent"] },
];
class WorkingModule {
}
WorkingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: WorkingModule });
WorkingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function WorkingModule_Factory(t) { return new (t || WorkingModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_5__["DisplayModule"],
            _angular_material_list__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
            _angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"].forChild(routes)
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](WorkingModule, { declarations: [_working_facade_component__WEBPACK_IMPORTED_MODULE_6__["WorkingFacadeComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_5__["DisplayModule"],
        _angular_material_list__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
        _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
        _angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](WorkingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [
                    _working_facade_component__WEBPACK_IMPORTED_MODULE_6__["WorkingFacadeComponent"],
                ],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_5__["DisplayModule"],
                    _angular_material_list__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                    _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                    _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
                    _angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"],
                    _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"].forChild(routes)
                ]
            }]
    }], null, null); })();


/***/ }),

/***/ "DrwV":
/*!**************************************************************************************************!*\
  !*** ./src/app/configuration/display-configuration-form/display-configuration-form.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: DisplayConfigurationFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisplayConfigurationFormComponent", function() { return DisplayConfigurationFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/checkbox */ "bSwM");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/radio */ "QibW");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var src_app_Models_Display_Model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/Models/Display.Model */ "QX5R");
/* harmony import */ var src_app_Models_DisplayInterfaces_Enum__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/Models/DisplayInterfaces.Enum */ "wUxT");
/* harmony import */ var src_app_Services_configuration_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/Services/configuration.service */ "JYHc");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/input */ "qFsG");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/form-field */ "kmnG");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/button */ "bTqV");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/list */ "MutI");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ "ofXK");


















function DisplayConfigurationFormComponent_mat_list_item_46_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-list-item", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "input", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DisplayConfigurationFormComponent_mat_list_item_46_Template_input_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const i_r2 = ctx.index; const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.onInterfaceSelected(i_r2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const i_r2 = ctx.index;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formControlName", i_r2);
} }
class DisplayConfigurationFormComponent {
    constructor(formBuilder, configurationService) {
        this.formBuilder = formBuilder;
        this.configurationService = configurationService;
        //Regex to validate "<ip>:<Port>""
        this.IpPortRegex = '^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]):(0|[1-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$';
        //Regex to Validate "COM<number>"
        this.ComPortRegex = '[C|c][O|o][M|m][0-9]+$';
        //Regex to Validate 1 oder 2
        this.GtxRowsRegex = '^((1)|(2))$';
        //Regex to Validate 16 oder 24
        this.GtxTextLengthRegex = '^((16)|(24))$';
        this.subscription = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subscription"];
        this.display = new src_app_Models_Display_Model__WEBPACK_IMPORTED_MODULE_5__["IDisplay"]();
        this.selectedDisplayIndex = -1;
        this.selectedInterfaceIndex = -1;
        this.displayConfig = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    ngOnInit() {
        this.form = this.formBuilder.group({
            id: [{ value: '' }],
            name: [{ value: '' }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            useAccidentsThisYear: [{ value: false }],
            useAccidentsLastYear: [{ value: false }],
            useDaysWithoutAccidents: [{ value: false }],
            useDaysWithoutAccidentsHighscore: [{ value: false }],
            useGtxText: [{ value: false }],
            gtxRows: [{ value: 1, disabled: !this.display.useGtxText }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.GtxRowsRegex)],
            gtxTextLength: [{ value: 16, disabled: !this.display.useGtxText }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.GtxTextLengthRegex)],
            displayInterface: [{ value: src_app_Models_DisplayInterfaces_Enum__WEBPACK_IMPORTED_MODULE_6__["DisplayInterfaces"].Network }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            // interfaces: [{value: new FormArray([]), disabled: !this.canEdit}],
            interfaces: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"]([]),
            manualModus: [{ value: true }],
        });
        this.getData();
    }
    getData() {
        setTimeout(() => {
            this.subscription = this.configurationService.startedEditing
                .subscribe((index) => {
                this.selectedDisplayIndex = index;
                this.display = this.configurationService.getDisplay(index);
                this.form.patchValue(this.display);
                this.onUseGtxTextChanged();
                while (this.form.controls.interfaces.length !== 0) {
                    this.form.controls.interfaces.removeAt(0);
                }
                this.display.interfaces.forEach(element => {
                    this.form.controls.interfaces.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](element));
                });
                this.setInterfaceValidators();
            });
        }, 500);
        setTimeout(() => {
            this.form.valueChanges.subscribe(display => {
                let data = Object.assign(Object.assign({}, this.display), this.form.value);
                this.displayConfig.emit(data);
            });
        }, 500);
    }
    notOnlyWhitespaceValidator(control) {
        const isWhitespace = (control.value || '').trim().length === 0;
        const isValid = !isWhitespace;
        return isValid ? null : { 'whitespace': true };
    }
    get interfaces() {
        return this.form.get('interfaces');
    }
    onUseGtxTextChanged() {
        if (this.form.value.useGtxText) {
            this.form.controls.gtxRows.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.GtxRowsRegex), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['gtxRows'].enable();
            this.form.controls.gtxTextLength.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.GtxTextLengthRegex), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['gtxTextLength'].enable();
        }
        else {
            this.form.controls.gtxRows.clearValidators();
            this.form.controls['gtxRows'].disable();
            this.form.controls.gtxTextLength.clearValidators();
            this.form.controls['gtxTextLength'].disable();
        }
    }
    getInterfaceControls() {
        return this.form.controls.interfaces.controls;
    }
    addInterface() {
        if (this.form.value.displayInterface == src_app_Models_DisplayInterfaces_Enum__WEBPACK_IMPORTED_MODULE_6__["DisplayInterfaces"].Network) {
            this.form.controls.interfaces.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.IpPortRegex), this.notOnlyWhitespaceValidator]));
        }
        else if (this.form.value.displayInterface == src_app_Models_DisplayInterfaces_Enum__WEBPACK_IMPORTED_MODULE_6__["DisplayInterfaces"].RS232) {
            this.form.controls.interfaces.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.ComPortRegex), this.notOnlyWhitespaceValidator]));
        }
    }
    onInterfaceSelected(i) {
        this.selectedInterfaceIndex = i;
    }
    deleteSelectedInterface() {
        this.form.controls.interfaces.removeAt(this.selectedInterfaceIndex);
        this.selectedInterfaceIndex = -1;
    }
    setInterfaceValidators() {
        if (this.form.value.displayInterface == src_app_Models_DisplayInterfaces_Enum__WEBPACK_IMPORTED_MODULE_6__["DisplayInterfaces"].Network) {
            this.interfaces.controls.forEach(inter => {
                inter.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.IpPortRegex), this.notOnlyWhitespaceValidator]);
                inter.updateValueAndValidity();
            });
        }
        else if (this.form.value.displayInterface == src_app_Models_DisplayInterfaces_Enum__WEBPACK_IMPORTED_MODULE_6__["DisplayInterfaces"].RS232) {
            this.interfaces.controls.forEach(inter => {
                inter.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.ComPortRegex), this.notOnlyWhitespaceValidator]);
                inter.updateValueAndValidity();
            });
        }
    }
    onReset() {
        this.configurationService.startedEditing.next(this.selectedDisplayIndex);
    }
    onSubmit() {
        let data = Object.assign(Object.assign({}, this.display), this.form.value);
        this.configurationService.saveDisplay(this.selectedDisplayIndex, data);
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
DisplayConfigurationFormComponent.ɵfac = function DisplayConfigurationFormComponent_Factory(t) { return new (t || DisplayConfigurationFormComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_Services_configuration_service__WEBPACK_IMPORTED_MODULE_7__["ConfigurationService"])); };
DisplayConfigurationFormComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: DisplayConfigurationFormComponent, selectors: [["app-display-configuration-form"]], outputs: { displayConfig: "displayConfig" }, features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵProvidersFeature"]([
            {
                provide: _angular_material_radio__WEBPACK_IMPORTED_MODULE_3__["MAT_RADIO_DEFAULT_OPTIONS"],
                useValue: { color: 'primary' },
            },
            {
                provide: _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_2__["MAT_CHECKBOX_DEFAULT_OPTIONS"],
                useValue: { color: 'primary' },
            }
        ])], decls: 57, vars: 5, consts: [[3, "formGroup", "ngSubmit"], [1, "outlineBorder"], [1, "header"], ["matInput", "", "type", "text", "formControlName", "id", "hidden", "", 1, "form-control"], [1, "group"], ["for", "name", 1, "subsectionHeader"], ["matInput", "", "type", "text", "formControlName", "name", 1, "form-control"], [1, "subsectionHeader"], ["type", "checkbox", "formControlName", "useAccidentsThisYear", 1, "form-control"], ["type", "checkbox", "formControlName", "useAccidentsLastYear", 1, "form-control"], ["type", "checkbox", "formControlName", "useDaysWithoutAccidents", 1, "form-control"], ["type", "checkbox", "formControlName", "useDaysWithoutAccidentsHighscore", 1, "form-control"], ["type", "checkbox", "formControlName", "useGtxText", 3, "change"], ["matInput", "", "type", "number", "formControlName", "gtxRows", 1, "form-control"], ["for", "gtxTextLength"], ["matInput", "", "type", "number", "formControlName", "gtxTextLength", 1, "form-control"], ["formControlName", "displayInterface", 1, "form-control", 3, "change"], ["value", "Network"], ["value", "RS232"], [1, "center", 2, "margin-bottom", "5px"], ["mat-raised-button", "", "color", "primary", "type", "button", 3, "click"], ["mat-raised-button", "", "color", "primary", "type", "button", 3, "disabled", "click"], ["formArrayName", "interfaces", 1, "form-control", "list", 3, "multiple"], ["class", "select", 4, "ngFor", "ngForOf"], ["type", "checkbox", "formControlName", "manualModus", "matTooltip", "Die Einstellung manueller Modus bestimmt, ob man eigene Werte in folgende Felder eintragen kann: Unf\u00E4lle dieses Jahr, Unf\u00E4lle letztes Jahr, Meisten Tage ohne Unfall, Meisten Tage ohne Unfall Rekord. 'an' = ja; 'aus' = nein", 1, "form-control"], [1, "group", "center"], ["mat-raised-button", "", "color", "primary", "type", "submit", 3, "disabled"], ["mat-raised-button", "", "color", "basic", "type", "button", 3, "click"], [1, "select"], ["matInput", "", "placeholder", "Schnittstelle", 1, "matlistitemInput", 3, "formControlName", "click"]], template: function DisplayConfigurationFormComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "form", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function DisplayConfigurationFormComponent_Template_form_ngSubmit_0_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h2", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Konfiguration:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "input", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-label", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Anzeigenname:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "input", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-label", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Features der Anzeige:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-checkbox", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Unf\u00E4lle dieses Jahr ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "mat-checkbox", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Unf\u00E4lle letztes Jahr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "mat-checkbox", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Tage seit dem letzten Unfall");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "mat-checkbox", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Meisten Tage ohne Unfall");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "mat-checkbox", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function DisplayConfigurationFormComponent_Template_mat_checkbox_change_21_listener() { return ctx.onUseGtxTextChanged(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, " Textanzeige");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Anzahl der Zeilen (1/2):");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "input", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "label", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Anzahl der Zeichen je Zeile (16/24):");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "input", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "mat-label", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Schnittstelle:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "mat-radio-group", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function DisplayConfigurationFormComponent_Template_mat_radio_group_change_34_listener() { return ctx.setInterfaceValidators(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "mat-radio-button", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "Netzwerkschnittstelle");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "mat-radio-button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Serielleschnittstelle");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DisplayConfigurationFormComponent_Template_button_click_41_listener() { return ctx.addInterface(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Neues Interface");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "button", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DisplayConfigurationFormComponent_Template_button_click_43_listener() { return ctx.deleteSelectedInterface(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Interface l\u00F6schen");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "mat-selection-list", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](46, DisplayConfigurationFormComponent_mat_list_item_46_Template, 2, 1, "mat-list-item", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "mat-label", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "Sonstiges:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "mat-checkbox", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, " Manueller Modus");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "button", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Speichern");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "button", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DisplayConfigurationFormComponent_Template_button_click_55_listener() { return ctx.onReset(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "Zur\u00FCcksetzten");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", ctx.selectedInterfaceIndex == -1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("multiple", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.getInterfaceControls());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.form.valid);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_material_input__WEBPACK_IMPORTED_MODULE_8__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__["MatLabel"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_2__["MatCheckbox"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NumberValueAccessor"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_3__["MatRadioGroup"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_3__["MatRadioButton"], _angular_material_button__WEBPACK_IMPORTED_MODULE_10__["MatButton"], _angular_material_list__WEBPACK_IMPORTED_MODULE_11__["MatSelectionList"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArrayName"], _angular_common__WEBPACK_IMPORTED_MODULE_12__["NgForOf"], _angular_material_list__WEBPACK_IMPORTED_MODULE_11__["MatListItem"]], styles: ["mat-radio-button[_ngcontent-%COMP%] {\n  margin: 3px;\n}\n\nmat-checkbox[_ngcontent-%COMP%]     .mat-checkbox-inner-container {\n  width: 30px;\n  height: 30px;\n}\n\n.matlistitemInput[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.list[_ngcontent-%COMP%] {\n  max-height: 100px;\n  overflow: auto;\n}\n\n.checkbox[_ngcontent-%COMP%] {\n  color: #3F51B5;\n}\n\n.radio[_ngcontent-%COMP%] {\n  color: #3F51B5;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxkaXNwbGF5LWNvbmZpZ3VyYXRpb24tZm9ybS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLGNBQUE7QUFDSjs7QUFFQTtFQUNJLGNBQUE7QUFDSiIsImZpbGUiOiJkaXNwbGF5LWNvbmZpZ3VyYXRpb24tZm9ybS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm1hdC1yYWRpby1idXR0b257XHJcbiAgICBtYXJnaW46IDNweDtcclxufVxyXG5cclxubWF0LWNoZWNrYm94IDo6bmctZGVlcCAubWF0LWNoZWNrYm94LWlubmVyLWNvbnRhaW5lciB7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIGhlaWdodDogMzBweDtcclxufVxyXG5cclxuLm1hdGxpc3RpdGVtSW5wdXR7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmxpc3R7XHJcbiAgICBtYXgtaGVpZ2h0OiAxMDBweDtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG59XHJcblxyXG4uY2hlY2tib3h7XHJcbiAgICBjb2xvcjogIzNGNTFCNTtcclxufVxyXG5cclxuLnJhZGlve1xyXG4gICAgY29sb3I6ICMzRjUxQjU7XHJcbn0iXX0= */", "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb25maWd1cmF0aW9uLWZhY2FkZS5jb21wb25lbnQuc2NzcyJ9 */", "mat-selection-list[_ngcontent-%COMP%] {\n  max-height: 80vh;\n  overflow: auto;\n}\n\nmat-list-option[_ngcontent-%COMP%] {\n  border: solid 1px #CED4DA;\n}\n\nmat-list-option[_ngcontent-%COMP%]:hover {\n  background-color: #8898f7;\n}\n\n.mat-list-item.mat-list-single-selected-option[_ngcontent-%COMP%] {\n  background: #3F51B5;\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxsaXN0LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLHlCQUFBO0FBQ0o7O0FBR0E7RUFDSSx5QkFBQTtBQUFKOztBQUdBO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0FBQUoiLCJmaWxlIjoibGlzdC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibWF0LXNlbGVjdGlvbi1saXN0e1xyXG4gICAgbWF4LWhlaWdodDogODB2aDtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gIH1cclxuXHJcbm1hdC1saXN0LW9wdGlvbiB7XHJcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjQ0VENERBO1xyXG4gICAgXHJcbn1cclxuXHJcbm1hdC1saXN0LW9wdGlvbjpob3ZlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM4ODk4Zjc7XHJcbn1cclxuXHJcbi5tYXQtbGlzdC1pdGVtLm1hdC1saXN0LXNpbmdsZS1zZWxlY3RlZC1vcHRpb24ge1xyXG4gICAgYmFja2dyb3VuZDogIzNGNTFCNTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcblxyXG5cclxuIl19 */", ".col[_ngcontent-%COMP%] {\n  height: 100hv;\n}\n\n.row[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.subsectionHeader[_ngcontent-%COMP%] {\n  font-size: 19px;\n}\n\n.group[_ngcontent-%COMP%] {\n  margin-bottom: 7px;\n}\n\n.header[_ngcontent-%COMP%] {\n  text-decoration: underline;\n  text-align: center;\n  font-size: 24px;\n}\n\n.center[_ngcontent-%COMP%] {\n  width: 100%;\n  display: block;\n  text-align: center;\n}\n\nbutton[_ngcontent-%COMP%] {\n  margin-right: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxsYXlvdXQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7QUFDSiIsImZpbGUiOiJsYXlvdXQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb2wge1xyXG4gICAgaGVpZ2h0OiAxMDBodjtcclxufVxyXG5cclxuLnJvd3tcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uc3Vic2VjdGlvbkhlYWRlcntcclxuICAgIGZvbnQtc2l6ZTogMTlweCA7XHJcbn1cclxuXHJcbi5ncm91cHtcclxuICAgIG1hcmdpbi1ib3R0b206IDdweDtcclxufVxyXG5cclxuLmhlYWRlcntcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAyNHB4O1xyXG59XHJcblxyXG4uY2VudGVye1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuYnV0dG9ue1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn0iXX0= */", ".outlineBorder[_ngcontent-%COMP%] {\n  margin: 2px;\n  border: solid 1px #CED4DA;\n  padding: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxib3guc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7QUFDSiIsImZpbGUiOiJib3guc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5vdXRsaW5lQm9yZGVye1xyXG4gICAgbWFyZ2luOiAycHg7XHJcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjQ0VENERBO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG59XHJcbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DisplayConfigurationFormComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-display-configuration-form',
                templateUrl: './display-configuration-form.component.html',
                styleUrls: [
                    './display-configuration-form.component.scss',
                    '../configuration-facade.component.scss',
                    '../../styles/global/list.scss',
                    '../../styles/global/layout.scss',
                    '../../styles/global/box.scss'
                ],
                providers: [
                    {
                        provide: _angular_material_radio__WEBPACK_IMPORTED_MODULE_3__["MAT_RADIO_DEFAULT_OPTIONS"],
                        useValue: { color: 'primary' },
                    },
                    {
                        provide: _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_2__["MAT_CHECKBOX_DEFAULT_OPTIONS"],
                        useValue: { color: 'primary' },
                    }
                ]
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: src_app_Services_configuration_service__WEBPACK_IMPORTED_MODULE_7__["ConfigurationService"] }]; }, { displayConfig: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }] }); })();


/***/ }),

/***/ "JYHc":
/*!***************************************************!*\
  !*** ./src/app/Services/configuration.service.ts ***!
  \***************************************************/
/*! exports provided: ConfigurationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigurationService", function() { return ConfigurationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var src_app_Models_Display_Model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Models/Display.Model */ "QX5R");
/* harmony import */ var src_app_Models_DisplayContent_Model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Models/DisplayContent.Model */ "mpes");
/* harmony import */ var src_app_Models_DisplayInterfaces_Enum__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Models/DisplayInterfaces.Enum */ "wUxT");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _app_config_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-config.service */ "89mU");








class ConfigurationService {
    constructor(http, appConfig) {
        this.http = http;
        this.appConfig = appConfig;
        this.startedEditing = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.displaysChanged = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.displayCounter = 0;
        this.webAPIServer = appConfig.webAPIServer;
    }
    initialize() {
        this.http.get(this.webAPIServer + "/api/display/get-all").subscribe(data => {
            this.displays = [...data.displays];
        });
    }
    triggerDisplaysChanged() {
        this.displaysChanged.next(this.displays);
    }
    duplicateDisplay(index) {
        let display = this.displays[index];
        display._id = "";
        this.displays.push(display);
        this.http.post(this.webAPIServer + "/api/display/add/", display).subscribe(res => {
            display._id = res._id;
            console.log(res.message);
        });
        this.displaysChanged.next(this.displays);
    }
    getDisplay(index) {
        return this.displays[index];
    }
    newDisplay() {
        this.displays.push(this.createEmptyDisplay());
        this.displaysChanged.next(this.displays.slice());
    }
    saveDisplay(index, newDisplay) {
        this.displays[index] = newDisplay;
        if (newDisplay._id == "") {
            this.http.post(this.webAPIServer + "/api/display/add/", newDisplay).subscribe(res => {
                newDisplay._id = res._id;
                console.log(res.message);
            });
        }
        else {
            this.http.patch(this.webAPIServer + "/api/display/update/" + newDisplay._id, newDisplay).subscribe(res => {
                console.log(res.message);
            });
        }
        this.displaysChanged.next(this.displays);
    }
    deleteDisplay(index) {
        this.http.delete(this.webAPIServer + "/api/display/delete/" + this.displays[index]._id).subscribe(res => {
            console.log(res.message);
        });
        this.displays.splice(index, 1);
        this.displaysChanged.next(this.displays.slice());
    }
    createEmptyDisplay() {
        var newDisplay = new src_app_Models_Display_Model__WEBPACK_IMPORTED_MODULE_2__["IDisplay"]();
        newDisplay._id = "";
        newDisplay.name = "Neues Display " + (this.displays.length + 1);
        newDisplay.useAccidentsLastYear = false;
        newDisplay.useAccidentsThisYear = false;
        newDisplay.useDaysWithoutAccidents = false;
        newDisplay.useDaysWithoutAccidentsHighscore = false;
        newDisplay.useGtxText = false;
        newDisplay.gtxRows = 1;
        newDisplay.gtxTextLength = 16;
        newDisplay.displayInterface = src_app_Models_DisplayInterfaces_Enum__WEBPACK_IMPORTED_MODULE_4__["DisplayInterfaces"].Network;
        newDisplay.interfaces = ['192.168.0.0:10001'];
        newDisplay.manualModus = false;
        newDisplay.displayContent = new src_app_Models_DisplayContent_Model__WEBPACK_IMPORTED_MODULE_3__["DisplayContent"]();
        return newDisplay;
    }
}
ConfigurationService.ɵfac = function ConfigurationService_Factory(t) { return new (t || ConfigurationService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_app_config_service__WEBPACK_IMPORTED_MODULE_6__["AppConfigService"])); };
ConfigurationService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ConfigurationService, factory: ConfigurationService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ConfigurationService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"] }, { type: _app_config_service__WEBPACK_IMPORTED_MODULE_6__["AppConfigService"] }]; }, null); })();


/***/ }),

/***/ "PkOl":
/*!*******************************************!*\
  !*** ./src/app/monitor/monitor.module.ts ***!
  \*******************************************/
/*! exports provided: MonitorModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MonitorModule", function() { return MonitorModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _monitor_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./monitor.component */ "sVpJ");
/* harmony import */ var _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared-modules/display/display.module */ "Z/RF");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/list */ "MutI");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/button */ "bTqV");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/icon */ "NFeN");










const routes = [
    { path: 'Monitor', component: _monitor_component__WEBPACK_IMPORTED_MODULE_2__["MonitorComponent"] }
];
class MonitorModule {
}
MonitorModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: MonitorModule });
MonitorModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function MonitorModule_Factory(t) { return new (t || MonitorModule)(); }, imports: [[
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_3__["DisplayModule"],
            _angular_material_list__WEBPACK_IMPORTED_MODULE_5__["MatListModule"],
            _angular_material_button__WEBPACK_IMPORTED_MODULE_6__["MatButtonModule"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MonitorModule, { declarations: [_monitor_component__WEBPACK_IMPORTED_MODULE_2__["MonitorComponent"]], imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_3__["DisplayModule"],
        _angular_material_list__WEBPACK_IMPORTED_MODULE_5__["MatListModule"],
        _angular_material_button__WEBPACK_IMPORTED_MODULE_6__["MatButtonModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MonitorModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [_monitor_component__WEBPACK_IMPORTED_MODULE_2__["MonitorComponent"]],
                imports: [
                    _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_3__["DisplayModule"],
                    _angular_material_list__WEBPACK_IMPORTED_MODULE_5__["MatListModule"],
                    _angular_material_button__WEBPACK_IMPORTED_MODULE_6__["MatButtonModule"],
                    _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"]
                ]
            }]
    }], null, null); })();


/***/ }),

/***/ "QX5R":
/*!*****************************************!*\
  !*** ./src/app/Models/Display.Model.ts ***!
  \*****************************************/
/*! exports provided: IDisplay */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IDisplay", function() { return IDisplay; });
/* harmony import */ var _DisplayContent_Model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DisplayContent.Model */ "mpes");
/* harmony import */ var _DisplayInterfaces_Enum__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DisplayInterfaces.Enum */ "wUxT");


class IDisplay {
    constructor() {
        this._id = "",
            this.name = "",
            this.useAccidentsLastYear = false,
            this.useAccidentsThisYear = false,
            this.useDaysWithoutAccidents = false,
            this.useDaysWithoutAccidentsHighscore = false,
            this.useGtxText = false,
            this.gtxRows = 2,
            this.gtxTextLength = 16,
            this.displayInterface = _DisplayInterfaces_Enum__WEBPACK_IMPORTED_MODULE_1__["DisplayInterfaces"].Network,
            this.interfaces = [],
            this.manualModus = false,
            this.time = "00:00",
            this.date = "01.01.01",
            this.displayContent = new _DisplayContent_Model__WEBPACK_IMPORTED_MODULE_0__["DisplayContent"]();
    }
}


/***/ }),

/***/ "RnhZ":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "K/tc",
	"./af.js": "K/tc",
	"./ar": "jnO4",
	"./ar-dz": "o1bE",
	"./ar-dz.js": "o1bE",
	"./ar-kw": "Qj4J",
	"./ar-kw.js": "Qj4J",
	"./ar-ly": "HP3h",
	"./ar-ly.js": "HP3h",
	"./ar-ma": "CoRJ",
	"./ar-ma.js": "CoRJ",
	"./ar-sa": "gjCT",
	"./ar-sa.js": "gjCT",
	"./ar-tn": "bYM6",
	"./ar-tn.js": "bYM6",
	"./ar.js": "jnO4",
	"./az": "SFxW",
	"./az.js": "SFxW",
	"./be": "H8ED",
	"./be.js": "H8ED",
	"./bg": "hKrs",
	"./bg.js": "hKrs",
	"./bm": "p/rL",
	"./bm.js": "p/rL",
	"./bn": "kEOa",
	"./bn-bd": "loYQ",
	"./bn-bd.js": "loYQ",
	"./bn.js": "kEOa",
	"./bo": "0mo+",
	"./bo.js": "0mo+",
	"./br": "aIdf",
	"./br.js": "aIdf",
	"./bs": "JVSJ",
	"./bs.js": "JVSJ",
	"./ca": "1xZ4",
	"./ca.js": "1xZ4",
	"./cs": "PA2r",
	"./cs.js": "PA2r",
	"./cv": "A+xa",
	"./cv.js": "A+xa",
	"./cy": "l5ep",
	"./cy.js": "l5ep",
	"./da": "DxQv",
	"./da.js": "DxQv",
	"./de": "tGlX",
	"./de-at": "s+uk",
	"./de-at.js": "s+uk",
	"./de-ch": "u3GI",
	"./de-ch.js": "u3GI",
	"./de.js": "tGlX",
	"./dv": "WYrj",
	"./dv.js": "WYrj",
	"./el": "jUeY",
	"./el.js": "jUeY",
	"./en-au": "Dmvi",
	"./en-au.js": "Dmvi",
	"./en-ca": "OIYi",
	"./en-ca.js": "OIYi",
	"./en-gb": "Oaa7",
	"./en-gb.js": "Oaa7",
	"./en-ie": "4dOw",
	"./en-ie.js": "4dOw",
	"./en-il": "czMo",
	"./en-il.js": "czMo",
	"./en-in": "7C5Q",
	"./en-in.js": "7C5Q",
	"./en-nz": "b1Dy",
	"./en-nz.js": "b1Dy",
	"./en-sg": "t+mt",
	"./en-sg.js": "t+mt",
	"./eo": "Zduo",
	"./eo.js": "Zduo",
	"./es": "iYuL",
	"./es-do": "CjzT",
	"./es-do.js": "CjzT",
	"./es-mx": "tbfe",
	"./es-mx.js": "tbfe",
	"./es-us": "Vclq",
	"./es-us.js": "Vclq",
	"./es.js": "iYuL",
	"./et": "7BjC",
	"./et.js": "7BjC",
	"./eu": "D/JM",
	"./eu.js": "D/JM",
	"./fa": "jfSC",
	"./fa.js": "jfSC",
	"./fi": "gekB",
	"./fi.js": "gekB",
	"./fil": "1ppg",
	"./fil.js": "1ppg",
	"./fo": "ByF4",
	"./fo.js": "ByF4",
	"./fr": "nyYc",
	"./fr-ca": "2fjn",
	"./fr-ca.js": "2fjn",
	"./fr-ch": "Dkky",
	"./fr-ch.js": "Dkky",
	"./fr.js": "nyYc",
	"./fy": "cRix",
	"./fy.js": "cRix",
	"./ga": "USCx",
	"./ga.js": "USCx",
	"./gd": "9rRi",
	"./gd.js": "9rRi",
	"./gl": "iEDd",
	"./gl.js": "iEDd",
	"./gom-deva": "qvJo",
	"./gom-deva.js": "qvJo",
	"./gom-latn": "DKr+",
	"./gom-latn.js": "DKr+",
	"./gu": "4MV3",
	"./gu.js": "4MV3",
	"./he": "x6pH",
	"./he.js": "x6pH",
	"./hi": "3E1r",
	"./hi.js": "3E1r",
	"./hr": "S6ln",
	"./hr.js": "S6ln",
	"./hu": "WxRl",
	"./hu.js": "WxRl",
	"./hy-am": "1rYy",
	"./hy-am.js": "1rYy",
	"./id": "UDhR",
	"./id.js": "UDhR",
	"./is": "BVg3",
	"./is.js": "BVg3",
	"./it": "bpih",
	"./it-ch": "bxKX",
	"./it-ch.js": "bxKX",
	"./it.js": "bpih",
	"./ja": "B55N",
	"./ja.js": "B55N",
	"./jv": "tUCv",
	"./jv.js": "tUCv",
	"./ka": "IBtZ",
	"./ka.js": "IBtZ",
	"./kk": "bXm7",
	"./kk.js": "bXm7",
	"./km": "6B0Y",
	"./km.js": "6B0Y",
	"./kn": "PpIw",
	"./kn.js": "PpIw",
	"./ko": "Ivi+",
	"./ko.js": "Ivi+",
	"./ku": "JCF/",
	"./ku.js": "JCF/",
	"./ky": "lgnt",
	"./ky.js": "lgnt",
	"./lb": "RAwQ",
	"./lb.js": "RAwQ",
	"./lo": "sp3z",
	"./lo.js": "sp3z",
	"./lt": "JvlW",
	"./lt.js": "JvlW",
	"./lv": "uXwI",
	"./lv.js": "uXwI",
	"./me": "KTz0",
	"./me.js": "KTz0",
	"./mi": "aIsn",
	"./mi.js": "aIsn",
	"./mk": "aQkU",
	"./mk.js": "aQkU",
	"./ml": "AvvY",
	"./ml.js": "AvvY",
	"./mn": "lYtQ",
	"./mn.js": "lYtQ",
	"./mr": "Ob0Z",
	"./mr.js": "Ob0Z",
	"./ms": "6+QB",
	"./ms-my": "ZAMP",
	"./ms-my.js": "ZAMP",
	"./ms.js": "6+QB",
	"./mt": "G0Uy",
	"./mt.js": "G0Uy",
	"./my": "honF",
	"./my.js": "honF",
	"./nb": "bOMt",
	"./nb.js": "bOMt",
	"./ne": "OjkT",
	"./ne.js": "OjkT",
	"./nl": "+s0g",
	"./nl-be": "2ykv",
	"./nl-be.js": "2ykv",
	"./nl.js": "+s0g",
	"./nn": "uEye",
	"./nn.js": "uEye",
	"./oc-lnc": "Fnuy",
	"./oc-lnc.js": "Fnuy",
	"./pa-in": "8/+R",
	"./pa-in.js": "8/+R",
	"./pl": "jVdC",
	"./pl.js": "jVdC",
	"./pt": "8mBD",
	"./pt-br": "0tRk",
	"./pt-br.js": "0tRk",
	"./pt.js": "8mBD",
	"./ro": "lyxo",
	"./ro.js": "lyxo",
	"./ru": "lXzo",
	"./ru.js": "lXzo",
	"./sd": "Z4QM",
	"./sd.js": "Z4QM",
	"./se": "//9w",
	"./se.js": "//9w",
	"./si": "7aV9",
	"./si.js": "7aV9",
	"./sk": "e+ae",
	"./sk.js": "e+ae",
	"./sl": "gVVK",
	"./sl.js": "gVVK",
	"./sq": "yPMs",
	"./sq.js": "yPMs",
	"./sr": "zx6S",
	"./sr-cyrl": "E+lV",
	"./sr-cyrl.js": "E+lV",
	"./sr.js": "zx6S",
	"./ss": "Ur1D",
	"./ss.js": "Ur1D",
	"./sv": "X709",
	"./sv.js": "X709",
	"./sw": "dNwA",
	"./sw.js": "dNwA",
	"./ta": "PeUW",
	"./ta.js": "PeUW",
	"./te": "XLvN",
	"./te.js": "XLvN",
	"./tet": "V2x9",
	"./tet.js": "V2x9",
	"./tg": "Oxv6",
	"./tg.js": "Oxv6",
	"./th": "EOgW",
	"./th.js": "EOgW",
	"./tk": "Wv91",
	"./tk.js": "Wv91",
	"./tl-ph": "Dzi0",
	"./tl-ph.js": "Dzi0",
	"./tlh": "z3Vd",
	"./tlh.js": "z3Vd",
	"./tr": "DoHr",
	"./tr.js": "DoHr",
	"./tzl": "z1FC",
	"./tzl.js": "z1FC",
	"./tzm": "wQk9",
	"./tzm-latn": "tT3J",
	"./tzm-latn.js": "tT3J",
	"./tzm.js": "wQk9",
	"./ug-cn": "YRex",
	"./ug-cn.js": "YRex",
	"./uk": "raLr",
	"./uk.js": "raLr",
	"./ur": "UpQW",
	"./ur.js": "UpQW",
	"./uz": "Loxo",
	"./uz-latn": "AQ68",
	"./uz-latn.js": "AQ68",
	"./uz.js": "Loxo",
	"./vi": "KSF8",
	"./vi.js": "KSF8",
	"./x-pseudo": "/X5v",
	"./x-pseudo.js": "/X5v",
	"./yo": "fzPg",
	"./yo.js": "fzPg",
	"./zh-cn": "XDpg",
	"./zh-cn.js": "XDpg",
	"./zh-hk": "SatO",
	"./zh-hk.js": "SatO",
	"./zh-mo": "OmwH",
	"./zh-mo.js": "OmwH",
	"./zh-tw": "kOpN",
	"./zh-tw.js": "kOpN"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "RnhZ";

/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./header/header.component */ "fECr");



class AppComponent {
    /**
     *
     */
    constructor() {
        this.title = 'UnfallAnzeigenFrontend';
    }
    ngOnInit() {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 1, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-header");
    } }, directives: [_header_header_component__WEBPACK_IMPORTED_MODULE_1__["HeaderComponent"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MifQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "VEDO":
/*!*********************************************************************************************!*\
  !*** ./src/app/shared-modules/display/accidents-this-year/accidents-this-year.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: AccidentsThisYearComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccidentsThisYearComponent", function() { return AccidentsThisYearComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/form-field */ "kmnG");
/* harmony import */ var _directives_number_only_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../directives/number-only.directive */ "ggr/");




class AccidentsThisYearComponent {
    constructor() {
        this.accidents = 0;
        this.accidentsChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.manuelModus = true;
    }
    ngOnInit() {
    }
    updateAccidents(event) {
        if (event !== undefined && event !== null) {
            if (event.target !== undefined && event.target !== null) {
                if (event.target.value !== undefined || event.target.value !== null) {
                    if (event.target.value === "") {
                        this.accidents = 0;
                        this.accidentsChange.emit(0);
                    }
                    else {
                        this.accidents = Number(event.target.value);
                        this.accidentsChange.emit(this.accidents);
                    }
                }
            }
        }
    }
}
AccidentsThisYearComponent.ɵfac = function AccidentsThisYearComponent_Factory(t) { return new (t || AccidentsThisYearComponent)(); };
AccidentsThisYearComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AccidentsThisYearComponent, selectors: [["app-accidents-this-year"]], inputs: { accidents: "accidents", manuelModus: "manuelModus" }, outputs: { accidentsChange: "accidentsChange" }, decls: 4, vars: 2, consts: [[1, "row"], [1, "col-8", "displayText", "displayLabel"], ["MatInput", "", "appNumberOnly", "", "maxlength", "2", 1, "col-4", "displayInput", "accident", 3, "value", "disabled", "change"]], template: function AccidentsThisYearComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-label", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Unf\u00E4lle dieses Jahr:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "input", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function AccidentsThisYearComponent_Template_input_change_3_listener($event) { return ctx.updateAccidents($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.accidents)("disabled", !ctx.manuelModus);
    } }, directives: [_angular_material_form_field__WEBPACK_IMPORTED_MODULE_1__["MatLabel"], _directives_number_only_directive__WEBPACK_IMPORTED_MODULE_2__["NumberOnlyDirective"]], styles: [".row[_ngcontent-%COMP%] {\n  margin: 2px;\n}\n\n.display[_ngcontent-%COMP%] {\n  background-color: black;\n  border-color: #a7a7a7;\n  border-width: 5px;\n  border-style: solid;\n  height: auto;\n  min-height: 50px;\n  padding: 3px;\n}\n\n.displayText[_ngcontent-%COMP%] {\n  background-color: black;\n  color: white;\n  align-items: center;\n}\n\n.displayHeader[_ngcontent-%COMP%] {\n  text-decoration: underline;\n  text-align: center;\n  font-size: 35px;\n  height: 60px;\n  width: 99%;\n  display: grid;\n}\n\n.displayLabel[_ngcontent-%COMP%] {\n  font-size: 30px;\n  display: flex;\n}\n\n.displayInput[_ngcontent-%COMP%] {\n  background-color: black;\n  border-color: #a7a7a7;\n  border-width: 2px;\n  border-style: solid;\n  color: red;\n  padding: 4px;\n  height: 50;\n  font-size: 40px;\n  -webkit-appearance: none;\n  -moz-appearance: none;\n}\n\n.accident[_ngcontent-%COMP%] {\n  text-align: right;\n  font-family: \"numeric\";\n}\n\n.notification[_ngcontent-%COMP%] {\n  font-family: \"editundot\";\n  text-align: center;\n  margin: 2px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxkaXNwbGF5LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksMEJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7QUFDSjs7QUFHQTtFQUNJLGVBQUE7RUFDQSxhQUFBO0FBQUo7O0FBR0E7RUFDSSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFFQSx3QkFBQTtFQUNBLHFCQUFBO0FBREo7O0FBSUE7RUFDSSxpQkFBQTtFQUNBLHNCQUFBO0FBREo7O0FBSUE7RUFDSSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQURKIiwiZmlsZSI6ImRpc3BsYXkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucm93IHtcclxuICAgIG1hcmdpbjogMnB4O1xyXG59XHJcblxyXG4uZGlzcGxheSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICAgIGJvcmRlci1jb2xvcjogcmdiKDE2NywgMTY3LCAxNjcpO1xyXG4gICAgYm9yZGVyLXdpZHRoOiA1cHg7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgbWluLWhlaWdodDogNTBweDtcclxuICAgIHBhZGRpbmc6IDNweDtcclxufVxyXG5cclxuLmRpc3BsYXlUZXh0e1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4uZGlzcGxheUhlYWRlcntcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAzNXB4O1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgd2lkdGg6IDk5JTtcclxuICAgIGRpc3BsYXk6IGdyaWQ7XHJcbn1cclxuXHJcblxyXG4uZGlzcGxheUxhYmVse1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuLmRpc3BsYXlJbnB1dHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMTY3LCAxNjcsIDE2Nyk7XHJcbiAgICBib3JkZXItd2lkdGg6IDJweDtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgcGFkZGluZzogNHB4O1xyXG4gICAgaGVpZ2h0OiA1MDtcclxuICAgIGZvbnQtc2l6ZTogNDBweDtcclxuXHJcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6bm9uZTsgXHJcbiAgICAtbW96LWFwcGVhcmFuY2U6IG5vbmU7IFxyXG59XHJcblxyXG4uYWNjaWRlbnQge1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBmb250LWZhbWlseTogJ251bWVyaWMnO1xyXG59XHJcblxyXG4ubm90aWZpY2F0aW9uIHtcclxuICAgIGZvbnQtZmFtaWx5OiAnZWRpdHVuZG90JztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMnB4O1xyXG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AccidentsThisYearComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-accidents-this-year',
                templateUrl: './accidents-this-year.component.html',
                styleUrls: [
                    '../display.component.scss'
                ]
            }]
    }], function () { return []; }, { accidents: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], accidentsChange: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }], manuelModus: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }] }); })();


/***/ }),

/***/ "VcEb":
/*!*********************************************!*\
  !*** ./src/app/Services/working.service.ts ***!
  \*********************************************/
/*! exports provided: WorkingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WorkingService", function() { return WorkingService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _app_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-config.service */ "89mU");





class WorkingService {
    constructor(http, appConfig) {
        this.http = http;
        this.appConfig = appConfig;
        this.selectedDisplaySubject = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.selectedDisplayIndex = -1;
        this.displaysChanged = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.displayCounter = 0;
        this.LogSubject = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.webAPIServer = appConfig.webAPIServer;
    }
    initialize() {
        this.http.get(this.webAPIServer + "/api/display/get-all").subscribe(data => {
            this.displays = [...data.displays];
        });
        this.selectedDisplaySubject.subscribe(index => this.selectedDisplayIndex = index);
    }
    triggerDisplaysChanged() {
        this.displaysChanged.next(this.displays);
    }
    interfaceSelected(selectedInterfaceIndex) {
        this.http.get(this.webAPIServer + "/api/display/get-accident-data/" + this.displays[this.selectedDisplayIndex]._id + "/" + selectedInterfaceIndex).subscribe(res => {
            if (res.status === 201) {
                this.displays[this.selectedDisplayIndex] = res.display;
                this.displaysChanged.next(this.displays);
            }
            else {
                this.updateLog(res.message);
            }
        }, err => {
            console.log(err);
            this.updateLog(err.message);
        });
    }
    updateLog(log) {
        if (log !== null || log !== undefined) {
            let logtext = "";
            logtext += log + "\n";
            logtext += "--------\n";
            this.LogSubject.next(logtext);
        }
    }
    reportAccident(display) {
        this.http.post(this.webAPIServer + "/api/display/report-accident/", display).subscribe(res => {
            if (res.status === 201) {
                this.displays[this.selectedDisplayIndex] = res.display;
                this.displaysChanged.next(this.displays);
            }
            this.updateLog(res.message);
        }, err => {
            this.updateLog(err.message);
        });
    }
    sendAccidentData(display) {
        this.http.post(this.webAPIServer + "/api/display/send-accident-data/", display).subscribe(res => {
            if (res.status === 201) {
                this.updateLog(res.message);
            }
            else {
                this.updateLog(res.message);
            }
        }, err => {
            this.updateLog(err.message);
        });
    }
    sendDateTimeData(display) {
        this.http.post(this.webAPIServer + "/api/display/send-date-time-data/", display).subscribe(res => {
            if (res.status === 201) {
                this.updateLog(res.message);
            }
            else {
                this.updateLog(res.message);
            }
        }, err => {
            this.updateLog(err.message);
        });
    }
}
WorkingService.ɵfac = function WorkingService_Factory(t) { return new (t || WorkingService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_app_config_service__WEBPACK_IMPORTED_MODULE_3__["AppConfigService"])); };
WorkingService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: WorkingService, factory: WorkingService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](WorkingService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }, { type: _app_config_service__WEBPACK_IMPORTED_MODULE_3__["AppConfigService"] }]; }, null); })();


/***/ }),

/***/ "Z/RF":
/*!**********************************************************!*\
  !*** ./src/app/shared-modules/display/display.module.ts ***!
  \**********************************************************/
/*! exports provided: DisplayModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisplayModule", function() { return DisplayModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/form-field */ "kmnG");
/* harmony import */ var _display_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./display.component */ "zzkD");
/* harmony import */ var _gtx_text_gtx_text_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./gtx-text/gtx-text.component */ "r2rP");
/* harmony import */ var _days_without_accidents_days_without_accidents_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./days-without-accidents/days-without-accidents.component */ "+QUk");
/* harmony import */ var _days_without_accidents_highscore_days_without_accidents_highscore_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./days-without-accidents-highscore/days-without-accidents-highscore.component */ "fnaR");
/* harmony import */ var _accidents_last_year_accidents_last_year_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./accidents-last-year/accidents-last-year.component */ "jyKa");
/* harmony import */ var _accidents_this_year_accidents_this_year_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./accidents-this-year/accidents-this-year.component */ "VEDO");
/* harmony import */ var _directives_number_only_directive__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./directives/number-only.directive */ "ggr/");











class DisplayModule {
}
DisplayModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: DisplayModule });
DisplayModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function DisplayModule_Factory(t) { return new (t || DisplayModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](DisplayModule, { declarations: [_display_component__WEBPACK_IMPORTED_MODULE_3__["DisplayComponent"],
        _gtx_text_gtx_text_component__WEBPACK_IMPORTED_MODULE_4__["GTXTextComponent"],
        _days_without_accidents_days_without_accidents_component__WEBPACK_IMPORTED_MODULE_5__["DaysWithoutAccidentsComponent"],
        _days_without_accidents_highscore_days_without_accidents_highscore_component__WEBPACK_IMPORTED_MODULE_6__["DaysWithoutAccidentsHighscoreComponent"],
        _accidents_last_year_accidents_last_year_component__WEBPACK_IMPORTED_MODULE_7__["AccidentsLastYearComponent"],
        _accidents_this_year_accidents_this_year_component__WEBPACK_IMPORTED_MODULE_8__["AccidentsThisYearComponent"],
        _directives_number_only_directive__WEBPACK_IMPORTED_MODULE_9__["NumberOnlyDirective"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"]], exports: [_display_component__WEBPACK_IMPORTED_MODULE_3__["DisplayComponent"],
        _gtx_text_gtx_text_component__WEBPACK_IMPORTED_MODULE_4__["GTXTextComponent"],
        _days_without_accidents_days_without_accidents_component__WEBPACK_IMPORTED_MODULE_5__["DaysWithoutAccidentsComponent"],
        _days_without_accidents_highscore_days_without_accidents_highscore_component__WEBPACK_IMPORTED_MODULE_6__["DaysWithoutAccidentsHighscoreComponent"],
        _accidents_last_year_accidents_last_year_component__WEBPACK_IMPORTED_MODULE_7__["AccidentsLastYearComponent"],
        _accidents_this_year_accidents_this_year_component__WEBPACK_IMPORTED_MODULE_8__["AccidentsThisYearComponent"],
        _directives_number_only_directive__WEBPACK_IMPORTED_MODULE_9__["NumberOnlyDirective"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DisplayModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [
                    _display_component__WEBPACK_IMPORTED_MODULE_3__["DisplayComponent"],
                    _gtx_text_gtx_text_component__WEBPACK_IMPORTED_MODULE_4__["GTXTextComponent"],
                    _days_without_accidents_days_without_accidents_component__WEBPACK_IMPORTED_MODULE_5__["DaysWithoutAccidentsComponent"],
                    _days_without_accidents_highscore_days_without_accidents_highscore_component__WEBPACK_IMPORTED_MODULE_6__["DaysWithoutAccidentsHighscoreComponent"],
                    _accidents_last_year_accidents_last_year_component__WEBPACK_IMPORTED_MODULE_7__["AccidentsLastYearComponent"],
                    _accidents_this_year_accidents_this_year_component__WEBPACK_IMPORTED_MODULE_8__["AccidentsThisYearComponent"],
                    _directives_number_only_directive__WEBPACK_IMPORTED_MODULE_9__["NumberOnlyDirective"],
                ],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                ],
                exports: [
                    _display_component__WEBPACK_IMPORTED_MODULE_3__["DisplayComponent"],
                    _gtx_text_gtx_text_component__WEBPACK_IMPORTED_MODULE_4__["GTXTextComponent"],
                    _days_without_accidents_days_without_accidents_component__WEBPACK_IMPORTED_MODULE_5__["DaysWithoutAccidentsComponent"],
                    _days_without_accidents_highscore_days_without_accidents_highscore_component__WEBPACK_IMPORTED_MODULE_6__["DaysWithoutAccidentsHighscoreComponent"],
                    _accidents_last_year_accidents_last_year_component__WEBPACK_IMPORTED_MODULE_7__["AccidentsLastYearComponent"],
                    _accidents_this_year_accidents_this_year_component__WEBPACK_IMPORTED_MODULE_8__["AccidentsThisYearComponent"],
                    _directives_number_only_directive__WEBPACK_IMPORTED_MODULE_9__["NumberOnlyDirective"],
                ]
            }]
    }], null, null); })();


/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "R1ws");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "lDzL");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./header/header.component */ "fECr");
/* harmony import */ var _Services_working_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./Services/working.service */ "VcEb");
/* harmony import */ var _Services_configuration_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./Services/configuration.service */ "JYHc");
/* harmony import */ var _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./shared-modules/display/display.module */ "Z/RF");
/* harmony import */ var _working_working_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./working/working.module */ "C98j");
/* harmony import */ var _configuration_configuration_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./configuration/configuration.module */ "xjgD");
/* harmony import */ var _monitor_monitor_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./monitor/monitor.module */ "PkOl");
/* harmony import */ var _layout_designer_layout_designer_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./layout-designer/layout-designer.module */ "vIyT");
/* harmony import */ var _Services_app_config_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./Services/app-config.service */ "89mU");
/* harmony import */ var _angular_common_locales_de__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/common/locales/de */ "VLs4");
/* harmony import */ var _angular_common_locales_de__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_angular_common_locales_de__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/toolbar */ "/t3+");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/menu */ "STbY");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/icon */ "NFeN");























Object(_angular_common__WEBPACK_IMPORTED_MODULE_18__["registerLocaleData"])(_angular_common_locales_de__WEBPACK_IMPORTED_MODULE_17___default.a);
class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [
        _Services_configuration_service__WEBPACK_IMPORTED_MODULE_10__["ConfigurationService"],
        _Services_working_service__WEBPACK_IMPORTED_MODULE_9__["WorkingService"],
        _Services_app_config_service__WEBPACK_IMPORTED_MODULE_16__["AppConfigService"],
        {
            provide: _angular_core__WEBPACK_IMPORTED_MODULE_0__["APP_INITIALIZER"],
            useFactory: (appConfigService) => () => appConfigService.loadAppConfig(),
            multi: true,
            deps: [_Services_app_config_service__WEBPACK_IMPORTED_MODULE_16__["AppConfigService"]]
        },
    ], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__["NgxDatatableModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            _working_working_module__WEBPACK_IMPORTED_MODULE_12__["WorkingModule"],
            _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_11__["DisplayModule"],
            _configuration_configuration_module__WEBPACK_IMPORTED_MODULE_13__["ConfigurationModule"],
            _monitor_monitor_module__WEBPACK_IMPORTED_MODULE_14__["MonitorModule"],
            _layout_designer_layout_designer_module__WEBPACK_IMPORTED_MODULE_15__["LayoutDesignerModule"],
            _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_19__["MatToolbarModule"],
            _angular_material_menu__WEBPACK_IMPORTED_MODULE_20__["MatMenuModule"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_21__["MatIconModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
        _header_header_component__WEBPACK_IMPORTED_MODULE_8__["HeaderComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
        _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
        _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__["NgxDatatableModule"],
        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
        _working_working_module__WEBPACK_IMPORTED_MODULE_12__["WorkingModule"],
        _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_11__["DisplayModule"],
        _configuration_configuration_module__WEBPACK_IMPORTED_MODULE_13__["ConfigurationModule"],
        _monitor_monitor_module__WEBPACK_IMPORTED_MODULE_14__["MonitorModule"],
        _layout_designer_layout_designer_module__WEBPACK_IMPORTED_MODULE_15__["LayoutDesignerModule"],
        _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_19__["MatToolbarModule"],
        _angular_material_menu__WEBPACK_IMPORTED_MODULE_20__["MatMenuModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_21__["MatIconModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                    _header_header_component__WEBPACK_IMPORTED_MODULE_8__["HeaderComponent"],
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                    _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                    _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__["NgxDatatableModule"],
                    _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                    _working_working_module__WEBPACK_IMPORTED_MODULE_12__["WorkingModule"],
                    _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_11__["DisplayModule"],
                    _configuration_configuration_module__WEBPACK_IMPORTED_MODULE_13__["ConfigurationModule"],
                    _monitor_monitor_module__WEBPACK_IMPORTED_MODULE_14__["MonitorModule"],
                    _layout_designer_layout_designer_module__WEBPACK_IMPORTED_MODULE_15__["LayoutDesignerModule"],
                    _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_19__["MatToolbarModule"],
                    _angular_material_menu__WEBPACK_IMPORTED_MODULE_20__["MatMenuModule"],
                    _angular_material_icon__WEBPACK_IMPORTED_MODULE_21__["MatIconModule"]
                ],
                providers: [
                    _Services_configuration_service__WEBPACK_IMPORTED_MODULE_10__["ConfigurationService"],
                    _Services_working_service__WEBPACK_IMPORTED_MODULE_9__["WorkingService"],
                    _Services_app_config_service__WEBPACK_IMPORTED_MODULE_16__["AppConfigService"],
                    {
                        provide: _angular_core__WEBPACK_IMPORTED_MODULE_0__["APP_INITIALIZER"],
                        useFactory: (appConfigService) => () => appConfigService.loadAppConfig(),
                        multi: true,
                        deps: [_Services_app_config_service__WEBPACK_IMPORTED_MODULE_16__["AppConfigService"]]
                    },
                ],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "fECr":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/toolbar */ "/t3+");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/menu */ "STbY");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/icon */ "NFeN");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");






class HeaderComponent {
    constructor() {
        this.PageTitle = "Unfallanzeigensteuerung";
    }
    ngOnInit() {
    }
    setPageTitle(title) {
        this.PageTitle = title;
    }
}
HeaderComponent.ɵfac = function HeaderComponent_Factory(t) { return new (t || HeaderComponent)(); };
HeaderComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HeaderComponent, selectors: [["app-header"]], decls: 19, vars: 2, consts: [["color", "primary", 2, "width", "100%"], ["src", "assets/Schauf_Logo.png", 2, "width", "70px", "height", "70px"], [1, "label"], ["mat-icon-button", "", 1, "menu", 3, "matMenuTriggerFor"], [1, "menuInhalt"], ["menu", "matMenu"], ["mat-menu-item", "", "routerLinkActive", "active", "routerLink", "/Anzeigen", 1, "menuInhalt", 3, "click"], ["mat-menu-item", "", "routerLinkActive", "active", "routerLink", "/Konfiguration", 1, "menuInhalt", 3, "click"], ["mat-menu-item", "", "routerLinkActive", "active", "routerLink", "/Monitor", 1, "menuInhalt", 3, "click"]], template: function HeaderComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-toolbar", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "label", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "menu");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-menu", 4, 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HeaderComponent_Template_button_click_9_listener() { return ctx.setPageTitle("Unfallanzeigensteuerung"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Unfallanzeigensteuerung");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HeaderComponent_Template_button_click_12_listener() { return ctx.setPageTitle("Konfiguration"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Konfiguration");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HeaderComponent_Template_button_click_15_listener() { return ctx.setPageTitle("Monitor"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Monitor");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "router-outlet");
    } if (rf & 2) {
        const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.PageTitle);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("matMenuTriggerFor", _r0);
    } }, directives: [_angular_material_toolbar__WEBPACK_IMPORTED_MODULE_1__["MatToolbar"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_2__["MatMenuTrigger"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__["MatIcon"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_2__["MatMenu"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_2__["MatMenuItem"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkActive"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLink"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterOutlet"]], styles: ["mat-toolbar {\n  color: \"primary\";\n}\n\n.menu {\n  background-color: #3F51B5;\n  color: white;\n  border: none;\n}\n\n.menuInhalt {\n  background-color: #3F51B5 !important;\n  color: white;\n  border: none;\n  border-style: none;\n  border-color: black;\n  outline: none;\n  lighting-color: black;\n}\n\n.menuInhalt:focus {\n  outline: none;\n  box-shadow: none;\n}\n\n.menu:focus {\n  outline: none;\n  box-shadow: none;\n}\n\nspan {\n  margin: 5px;\n}\n\n.label {\n  width: 100%;\n  display: block;\n  text-align: center;\n  font-size: 32px;\n  font-weight: normal;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGhlYWRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBSUE7RUFDSSxvQ0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtBQURKOztBQUlBO0VBQ0ksYUFBQTtFQUNBLGdCQUFBO0FBREo7O0FBR0E7RUFDSSxhQUFBO0VBQ0EsZ0JBQUE7QUFBSjs7QUFHQTtFQUNJLFdBQUE7QUFBSjs7QUFHQTtFQUNJLFdBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUFBSiIsImZpbGUiOiJoZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJtYXQtdG9vbGJhcntcclxuICAgIGNvbG9yOiBcInByaW1hcnlcIjtcclxufVxyXG5cclxuLm1lbnUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNGNTFCNTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuXHJcbn1cclxuXHJcblxyXG4ubWVudUluaGFsdHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzRjUxQjUgIWltcG9ydGFudDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGJvcmRlci1zdHlsZTogbm9uZTtcclxuICAgIGJvcmRlci1jb2xvcjogYmxhY2s7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgbGlnaHRpbmctY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4ubWVudUluaGFsdDpmb2N1c3tcclxuICAgIG91dGxpbmU6bm9uZTtcclxuICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbn1cclxuLm1lbnU6Zm9jdXN7XHJcbiAgICBvdXRsaW5lOm5vbmU7XHJcbiAgICBib3gtc2hhZG93OiBub25lO1xyXG59XHJcblxyXG5zcGFue1xyXG4gICAgbWFyZ2luOiA1cHg7XHJcbn1cclxuXHJcbi5sYWJlbHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDMycHg7XHJcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG59Il19 */"], encapsulation: 2 });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HeaderComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-header',
                templateUrl: './header.component.html',
                styleUrls: ['./header.component.scss'],
                encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "fnaR":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/shared-modules/display/days-without-accidents-highscore/days-without-accidents-highscore.component.ts ***!
  \***********************************************************************************************************************/
/*! exports provided: DaysWithoutAccidentsHighscoreComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DaysWithoutAccidentsHighscoreComponent", function() { return DaysWithoutAccidentsHighscoreComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/form-field */ "kmnG");
/* harmony import */ var _directives_number_only_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../directives/number-only.directive */ "ggr/");




class DaysWithoutAccidentsHighscoreComponent {
    constructor() {
        this.accidents = 0;
        this.accidentsChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.manuelModus = true;
    }
    ngOnInit() {
    }
    updateAccidents(event) {
        if (event !== undefined && event !== null) {
            if (event.target !== undefined && event.target !== null) {
                if (event.target.value !== undefined || event.target.value !== null) {
                    if (event.target.value === "") {
                        this.accidents = 0;
                        this.accidentsChange.emit(0);
                    }
                    else {
                        this.accidents = Number(event.target.value);
                        this.accidentsChange.emit(this.accidents);
                    }
                }
            }
        }
    }
}
DaysWithoutAccidentsHighscoreComponent.ɵfac = function DaysWithoutAccidentsHighscoreComponent_Factory(t) { return new (t || DaysWithoutAccidentsHighscoreComponent)(); };
DaysWithoutAccidentsHighscoreComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: DaysWithoutAccidentsHighscoreComponent, selectors: [["app-days-without-accidents-highscore"]], inputs: { accidents: "accidents", manuelModus: "manuelModus" }, outputs: { accidentsChange: "accidentsChange" }, decls: 4, vars: 2, consts: [[1, "row"], [1, "col-8", "displayText", "displayLabel"], ["MatInput", "", "appNumberOnly", "", "maxlength", "4", 1, "col-4", "displayInput", "accident", 3, "value", "disabled", "change"]], template: function DaysWithoutAccidentsHighscoreComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-label", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Meisten Tage ohne Unfall:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "input", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function DaysWithoutAccidentsHighscoreComponent_Template_input_change_3_listener($event) { return ctx.updateAccidents($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.accidents)("disabled", !ctx.manuelModus);
    } }, directives: [_angular_material_form_field__WEBPACK_IMPORTED_MODULE_1__["MatLabel"], _directives_number_only_directive__WEBPACK_IMPORTED_MODULE_2__["NumberOnlyDirective"]], styles: [".row[_ngcontent-%COMP%] {\n  margin: 2px;\n}\n\n.display[_ngcontent-%COMP%] {\n  background-color: black;\n  border-color: #a7a7a7;\n  border-width: 5px;\n  border-style: solid;\n  height: auto;\n  min-height: 50px;\n  padding: 3px;\n}\n\n.displayText[_ngcontent-%COMP%] {\n  background-color: black;\n  color: white;\n  align-items: center;\n}\n\n.displayHeader[_ngcontent-%COMP%] {\n  text-decoration: underline;\n  text-align: center;\n  font-size: 35px;\n  height: 60px;\n  width: 99%;\n  display: grid;\n}\n\n.displayLabel[_ngcontent-%COMP%] {\n  font-size: 30px;\n  display: flex;\n}\n\n.displayInput[_ngcontent-%COMP%] {\n  background-color: black;\n  border-color: #a7a7a7;\n  border-width: 2px;\n  border-style: solid;\n  color: red;\n  padding: 4px;\n  height: 50;\n  font-size: 40px;\n  -webkit-appearance: none;\n  -moz-appearance: none;\n}\n\n.accident[_ngcontent-%COMP%] {\n  text-align: right;\n  font-family: \"numeric\";\n}\n\n.notification[_ngcontent-%COMP%] {\n  font-family: \"editundot\";\n  text-align: center;\n  margin: 2px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxkaXNwbGF5LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksMEJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7QUFDSjs7QUFHQTtFQUNJLGVBQUE7RUFDQSxhQUFBO0FBQUo7O0FBR0E7RUFDSSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFFQSx3QkFBQTtFQUNBLHFCQUFBO0FBREo7O0FBSUE7RUFDSSxpQkFBQTtFQUNBLHNCQUFBO0FBREo7O0FBSUE7RUFDSSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQURKIiwiZmlsZSI6ImRpc3BsYXkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucm93IHtcclxuICAgIG1hcmdpbjogMnB4O1xyXG59XHJcblxyXG4uZGlzcGxheSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICAgIGJvcmRlci1jb2xvcjogcmdiKDE2NywgMTY3LCAxNjcpO1xyXG4gICAgYm9yZGVyLXdpZHRoOiA1cHg7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgbWluLWhlaWdodDogNTBweDtcclxuICAgIHBhZGRpbmc6IDNweDtcclxufVxyXG5cclxuLmRpc3BsYXlUZXh0e1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4uZGlzcGxheUhlYWRlcntcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAzNXB4O1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgd2lkdGg6IDk5JTtcclxuICAgIGRpc3BsYXk6IGdyaWQ7XHJcbn1cclxuXHJcblxyXG4uZGlzcGxheUxhYmVse1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuLmRpc3BsYXlJbnB1dHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMTY3LCAxNjcsIDE2Nyk7XHJcbiAgICBib3JkZXItd2lkdGg6IDJweDtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgcGFkZGluZzogNHB4O1xyXG4gICAgaGVpZ2h0OiA1MDtcclxuICAgIGZvbnQtc2l6ZTogNDBweDtcclxuXHJcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6bm9uZTsgXHJcbiAgICAtbW96LWFwcGVhcmFuY2U6IG5vbmU7IFxyXG59XHJcblxyXG4uYWNjaWRlbnQge1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBmb250LWZhbWlseTogJ251bWVyaWMnO1xyXG59XHJcblxyXG4ubm90aWZpY2F0aW9uIHtcclxuICAgIGZvbnQtZmFtaWx5OiAnZWRpdHVuZG90JztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMnB4O1xyXG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DaysWithoutAccidentsHighscoreComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-days-without-accidents-highscore',
                templateUrl: './days-without-accidents-highscore.component.html',
                styleUrls: [
                    '../display.component.scss'
                ]
            }]
    }], function () { return []; }, { accidents: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], accidentsChange: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }], manuelModus: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }] }); })();


/***/ }),

/***/ "ggr/":
/*!****************************************************************************!*\
  !*** ./src/app/shared-modules/display/directives/number-only.directive.ts ***!
  \****************************************************************************/
/*! exports provided: NumberOnlyDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumberOnlyDirective", function() { return NumberOnlyDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");


class NumberOnlyDirective {
    constructor(el) {
        this.el = el;
        // Allow decimal numbers. The \. is only allowed once to occur
        this.regex = new RegExp(/^[0-9]+(\.[0-9]*){0,1}$/g);
        // Allow key codes for special events. Reflect :
        // Backspace, tab, end, home
        this.specialKeys = ['Backspace', 'Tab', 'End', 'Home'];
    }
    onKeyDown(event) {
        // Allow Backspace, tab, end, and home keys
        if (this.specialKeys.indexOf(event.key) !== -1) {
            return;
        }
        // Do not use event.keycode this is deprecated.
        // See: https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/keyCode
        let current = this.el.nativeElement.value;
        // We need this because the current value on the DOM element
        // is not yet updated with the value from this event
        let next = current.concat(event.key);
        if (next && !String(next).match(this.regex)) {
            event.preventDefault();
        }
    }
}
NumberOnlyDirective.ɵfac = function NumberOnlyDirective_Factory(t) { return new (t || NumberOnlyDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])); };
NumberOnlyDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({ type: NumberOnlyDirective, selectors: [["", "appNumberOnly", ""]], hostBindings: function NumberOnlyDirective_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown", function NumberOnlyDirective_keydown_HostBindingHandler($event) { return ctx.onKeyDown($event); });
    } } });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NumberOnlyDirective, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
        args: [{
                selector: '[appNumberOnly]'
            }]
    }], function () { return [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }]; }, { onKeyDown: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['keydown', ['$event']]
        }] }); })();


/***/ }),

/***/ "jyKa":
/*!*********************************************************************************************!*\
  !*** ./src/app/shared-modules/display/accidents-last-year/accidents-last-year.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: AccidentsLastYearComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccidentsLastYearComponent", function() { return AccidentsLastYearComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/form-field */ "kmnG");
/* harmony import */ var _directives_number_only_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../directives/number-only.directive */ "ggr/");




class AccidentsLastYearComponent {
    constructor() {
        this.accidents = 0;
        this.accidentsChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.manuelModus = true;
    }
    ngOnInit() {
    }
    updateAccidents(event) {
        if (event !== undefined && event !== null) {
            if (event.target !== undefined && event.target !== null) {
                if (event.target.value !== undefined || event.target.value !== null) {
                    if (event.target.value === "") {
                        this.accidents = 0;
                        this.accidentsChange.emit(0);
                    }
                    else {
                        this.accidents = Number(event.target.value);
                        this.accidentsChange.emit(this.accidents);
                    }
                }
            }
        }
    }
}
AccidentsLastYearComponent.ɵfac = function AccidentsLastYearComponent_Factory(t) { return new (t || AccidentsLastYearComponent)(); };
AccidentsLastYearComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AccidentsLastYearComponent, selectors: [["app-accidents-last-year"]], inputs: { accidents: "accidents", manuelModus: "manuelModus" }, outputs: { accidentsChange: "accidentsChange" }, decls: 4, vars: 2, consts: [[1, "row"], [1, "col-8", "displayText", "displayLabel"], ["MatInput", "", "appNumberOnly", "", "maxlength", "2", 1, "col-4", "displayInput", "accident", 3, "value", "disabled", "change"]], template: function AccidentsLastYearComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-label", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Unf\u00E4lle letztes Jahr:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "input", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function AccidentsLastYearComponent_Template_input_change_3_listener($event) { return ctx.updateAccidents($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.accidents)("disabled", !ctx.manuelModus);
    } }, directives: [_angular_material_form_field__WEBPACK_IMPORTED_MODULE_1__["MatLabel"], _directives_number_only_directive__WEBPACK_IMPORTED_MODULE_2__["NumberOnlyDirective"]], styles: [".row[_ngcontent-%COMP%] {\n  margin: 2px;\n}\n\n.display[_ngcontent-%COMP%] {\n  background-color: black;\n  border-color: #a7a7a7;\n  border-width: 5px;\n  border-style: solid;\n  height: auto;\n  min-height: 50px;\n  padding: 3px;\n}\n\n.displayText[_ngcontent-%COMP%] {\n  background-color: black;\n  color: white;\n  align-items: center;\n}\n\n.displayHeader[_ngcontent-%COMP%] {\n  text-decoration: underline;\n  text-align: center;\n  font-size: 35px;\n  height: 60px;\n  width: 99%;\n  display: grid;\n}\n\n.displayLabel[_ngcontent-%COMP%] {\n  font-size: 30px;\n  display: flex;\n}\n\n.displayInput[_ngcontent-%COMP%] {\n  background-color: black;\n  border-color: #a7a7a7;\n  border-width: 2px;\n  border-style: solid;\n  color: red;\n  padding: 4px;\n  height: 50;\n  font-size: 40px;\n  -webkit-appearance: none;\n  -moz-appearance: none;\n}\n\n.accident[_ngcontent-%COMP%] {\n  text-align: right;\n  font-family: \"numeric\";\n}\n\n.notification[_ngcontent-%COMP%] {\n  font-family: \"editundot\";\n  text-align: center;\n  margin: 2px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxkaXNwbGF5LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksMEJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7QUFDSjs7QUFHQTtFQUNJLGVBQUE7RUFDQSxhQUFBO0FBQUo7O0FBR0E7RUFDSSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFFQSx3QkFBQTtFQUNBLHFCQUFBO0FBREo7O0FBSUE7RUFDSSxpQkFBQTtFQUNBLHNCQUFBO0FBREo7O0FBSUE7RUFDSSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQURKIiwiZmlsZSI6ImRpc3BsYXkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucm93IHtcclxuICAgIG1hcmdpbjogMnB4O1xyXG59XHJcblxyXG4uZGlzcGxheSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICAgIGJvcmRlci1jb2xvcjogcmdiKDE2NywgMTY3LCAxNjcpO1xyXG4gICAgYm9yZGVyLXdpZHRoOiA1cHg7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgbWluLWhlaWdodDogNTBweDtcclxuICAgIHBhZGRpbmc6IDNweDtcclxufVxyXG5cclxuLmRpc3BsYXlUZXh0e1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4uZGlzcGxheUhlYWRlcntcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAzNXB4O1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgd2lkdGg6IDk5JTtcclxuICAgIGRpc3BsYXk6IGdyaWQ7XHJcbn1cclxuXHJcblxyXG4uZGlzcGxheUxhYmVse1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuLmRpc3BsYXlJbnB1dHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMTY3LCAxNjcsIDE2Nyk7XHJcbiAgICBib3JkZXItd2lkdGg6IDJweDtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgcGFkZGluZzogNHB4O1xyXG4gICAgaGVpZ2h0OiA1MDtcclxuICAgIGZvbnQtc2l6ZTogNDBweDtcclxuXHJcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6bm9uZTsgXHJcbiAgICAtbW96LWFwcGVhcmFuY2U6IG5vbmU7IFxyXG59XHJcblxyXG4uYWNjaWRlbnQge1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBmb250LWZhbWlseTogJ251bWVyaWMnO1xyXG59XHJcblxyXG4ubm90aWZpY2F0aW9uIHtcclxuICAgIGZvbnQtZmFtaWx5OiAnZWRpdHVuZG90JztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMnB4O1xyXG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AccidentsLastYearComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-accidents-last-year',
                templateUrl: './accidents-last-year.component.html',
                styleUrls: [
                    '../display.component.scss'
                ],
            }]
    }], function () { return []; }, { accidents: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], accidentsChange: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }], manuelModus: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }] }); })();


/***/ }),

/***/ "mpes":
/*!************************************************!*\
  !*** ./src/app/Models/DisplayContent.Model.ts ***!
  \************************************************/
/*! exports provided: DisplayContent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisplayContent", function() { return DisplayContent; });
class DisplayContent {
    constructor(accidentsLastYear = 0, accidentsThisYear = 0, daysWithoutAccidents = 0, daysWithoutAccidentsHighscore = 0, gtxTextFirstRow = "", gtxTextSecoundRow = "") {
        this.accidentsLastYear = accidentsLastYear;
        this.accidentsThisYear = accidentsThisYear;
        this.daysWithoutAccidents = daysWithoutAccidents;
        this.daysWithoutAccidentsHighscore = daysWithoutAccidentsHighscore;
        this.gtxTextFirstRow = gtxTextFirstRow;
        this.gtxTextSecoundRow = gtxTextSecoundRow;
    }
}


/***/ }),

/***/ "r2rP":
/*!***********************************************************************!*\
  !*** ./src/app/shared-modules/display/gtx-text/gtx-text.component.ts ***!
  \***********************************************************************/
/*! exports provided: GTXTextComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GTXTextComponent", function() { return GTXTextComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "ofXK");



function GTXTextComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "input", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function GTXTextComponent_div_2_Template_input_change_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2); const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r1.updateTextRow($event, 2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx_r0.row2);
} }
class GTXTextComponent {
    constructor() {
        this.row1 = "Achtung!";
        this.row2 = "Bitte passen Sie auf.";
        this.rowsCount = 1;
        this.characterCount = 16;
        this.row1Change = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.row2Change = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    ngOnInit() {
    }
    updateTextRow(event, row) {
        if (event !== undefined && event !== null) {
            if (event.target !== undefined && event.target !== null) {
                if (event.target.value !== undefined || event.target.value !== null) {
                    if (row === 1) {
                        this.row1 = event.target.value;
                        this.row1Change.emit(this.row1);
                    }
                    else {
                        this.row2 = event.target.value;
                        this.row2Change.emit(this.row2);
                    }
                }
            }
        }
    }
}
GTXTextComponent.ɵfac = function GTXTextComponent_Factory(t) { return new (t || GTXTextComponent)(); };
GTXTextComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: GTXTextComponent, selectors: [["app-gtx-text"]], inputs: { row1: "row1", row2: "row2", rowsCount: "rowsCount", characterCount: "characterCount" }, outputs: { row1Change: "row1Change", row2Change: "row2Change" }, decls: 3, vars: 3, consts: [[1, "row"], ["matInput", "", 1, "displayInput", "notification", 3, "value", "change"], ["class", "row", 4, "ngIf"]], template: function GTXTextComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "input", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function GTXTextComponent_Template_input_change_1_listener($event) { return ctx.updateTextRow($event, 1); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, GTXTextComponent_div_2_Template, 2, 1, "div", 2);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.row1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("maxlength", ctx.rowsCount >= 2 ? ctx.characterCount : null);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.rowsCount >= 2);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"]], styles: [".row[_ngcontent-%COMP%] {\n  margin: 2px;\n}\n\n.display[_ngcontent-%COMP%] {\n  background-color: black;\n  border-color: #a7a7a7;\n  border-width: 5px;\n  border-style: solid;\n  height: auto;\n  min-height: 50px;\n  padding: 3px;\n}\n\n.displayText[_ngcontent-%COMP%] {\n  background-color: black;\n  color: white;\n  align-items: center;\n}\n\n.displayHeader[_ngcontent-%COMP%] {\n  text-decoration: underline;\n  text-align: center;\n  font-size: 35px;\n  height: 60px;\n  width: 99%;\n  display: grid;\n}\n\n.displayLabel[_ngcontent-%COMP%] {\n  font-size: 30px;\n  display: flex;\n}\n\n.displayInput[_ngcontent-%COMP%] {\n  background-color: black;\n  border-color: #a7a7a7;\n  border-width: 2px;\n  border-style: solid;\n  color: red;\n  padding: 4px;\n  height: 50;\n  font-size: 40px;\n  -webkit-appearance: none;\n  -moz-appearance: none;\n}\n\n.accident[_ngcontent-%COMP%] {\n  text-align: right;\n  font-family: \"numeric\";\n}\n\n.notification[_ngcontent-%COMP%] {\n  font-family: \"editundot\";\n  text-align: center;\n  margin: 2px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxkaXNwbGF5LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksMEJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7QUFDSjs7QUFHQTtFQUNJLGVBQUE7RUFDQSxhQUFBO0FBQUo7O0FBR0E7RUFDSSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFFQSx3QkFBQTtFQUNBLHFCQUFBO0FBREo7O0FBSUE7RUFDSSxpQkFBQTtFQUNBLHNCQUFBO0FBREo7O0FBSUE7RUFDSSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQURKIiwiZmlsZSI6ImRpc3BsYXkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucm93IHtcclxuICAgIG1hcmdpbjogMnB4O1xyXG59XHJcblxyXG4uZGlzcGxheSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICAgIGJvcmRlci1jb2xvcjogcmdiKDE2NywgMTY3LCAxNjcpO1xyXG4gICAgYm9yZGVyLXdpZHRoOiA1cHg7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgbWluLWhlaWdodDogNTBweDtcclxuICAgIHBhZGRpbmc6IDNweDtcclxufVxyXG5cclxuLmRpc3BsYXlUZXh0e1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4uZGlzcGxheUhlYWRlcntcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAzNXB4O1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgd2lkdGg6IDk5JTtcclxuICAgIGRpc3BsYXk6IGdyaWQ7XHJcbn1cclxuXHJcblxyXG4uZGlzcGxheUxhYmVse1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuLmRpc3BsYXlJbnB1dHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMTY3LCAxNjcsIDE2Nyk7XHJcbiAgICBib3JkZXItd2lkdGg6IDJweDtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgcGFkZGluZzogNHB4O1xyXG4gICAgaGVpZ2h0OiA1MDtcclxuICAgIGZvbnQtc2l6ZTogNDBweDtcclxuXHJcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6bm9uZTsgXHJcbiAgICAtbW96LWFwcGVhcmFuY2U6IG5vbmU7IFxyXG59XHJcblxyXG4uYWNjaWRlbnQge1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBmb250LWZhbWlseTogJ251bWVyaWMnO1xyXG59XHJcblxyXG4ubm90aWZpY2F0aW9uIHtcclxuICAgIGZvbnQtZmFtaWx5OiAnZWRpdHVuZG90JztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMnB4O1xyXG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](GTXTextComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-gtx-text',
                templateUrl: './gtx-text.component.html',
                styleUrls: [
                    '../display.component.scss'
                ]
            }]
    }], function () { return []; }, { row1: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ["row1"]
        }], row2: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ["row2"]
        }], rowsCount: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], characterCount: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], row1Change: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }], row2Change: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }] }); })();


/***/ }),

/***/ "sVpJ":
/*!**********************************************!*\
  !*** ./src/app/monitor/monitor.component.ts ***!
  \**********************************************/
/*! exports provided: MonitorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MonitorComponent", function() { return MonitorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! socket.io-client */ "jifJ");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Models_Display_Model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Models/Display.Model */ "QX5R");
/* harmony import */ var _Services_app_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Services/app-config.service */ "89mU");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _shared_modules_display_display_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared-modules/display/display.component */ "zzkD");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/button */ "bTqV");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/icon */ "NFeN");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/list */ "MutI");










function MonitorComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MonitorComponent_div_2_Template_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3); const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r2.hideDisplaySelectClicked(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-icon", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "navigate_next");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function MonitorComponent_div_3_mat_list_option_13_Template(rf, ctx) { if (rf & 1) {
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-list-option", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MonitorComponent_div_3_mat_list_option_13_Template_mat_list_option_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8); const i_r6 = ctx.index; const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r7.onSelectDisplay(i_r6); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const display_r5 = ctx.$implicit;
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("selected", display_r5._id == ctx_r4.selectedDisplayID);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](display_r5.name);
} }
function MonitorComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MonitorComponent_div_3_Template_button_click_3_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r10); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r9.reload(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "mat-icon", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "loop");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h2", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Anzeigen:");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MonitorComponent_div_3_Template_button_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r10); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r11.hideDisplaySelectClicked(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-icon", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "chevron_left");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-selection-list", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, MonitorComponent_div_3_mat_list_option_13_Template, 2, 2, "mat-list-option", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("multiple", false);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.displays);
} }
class MonitorComponent {
    constructor(appConfig) {
        this.appConfig = appConfig;
        this.selectedDisplay = new _Models_Display_Model__WEBPACK_IMPORTED_MODULE_2__["IDisplay"]();
        this.selectedDisplayID = "0";
        this.hideDisplaySelect = false;
        this.displayColClass = "col-9";
    }
    ngOnInit() {
        this.setupSocketConnection();
    }
    setupSocketConnection() {
        this.socket = Object(socket_io_client__WEBPACK_IMPORTED_MODULE_1__["io"])(this.appConfig.webAPIServer, {
            auth: {
                token: 'BSf@s=hP>W[aP$HP:5E*,m*e$1p3e5Nio{9fcT]A$f4BJ<KW,@9Y{n4jl`C9r8)|S1>I!z8a49:>OJ9VqmD%tOCC/r*3[;``CH{]'
            }
        });
        this.socket.emit("getDisplays");
        this.socket.on("sendDisplays", (newDisplays) => {
            this.displays = newDisplays;
            console.log(this.displays);
            if (this.displays.some(e => e._id === this.selectedDisplayID)) {
                this.selectedDisplay = this.displays.find(x => x._id === this.selectedDisplayID);
            }
            else {
                if (this.displays.length > 1) {
                    this.selectedDisplay = this.displays[0];
                    this.selectedDisplayID = this.selectedDisplay._id;
                }
            }
        });
        this.socket.on("displayChanged", (display) => {
            console.log(display);
            this.displays[this.displays.findIndex(x => x._id == display._id)] = display;
            if (this.selectedDisplayID == display._id) {
                this.selectedDisplay = display;
            }
        });
        this.socket.on("displayAdded", (display) => {
            this.displays.push(display);
        });
        this.socket.on("displayDeleted", (id) => {
            this.displays.splice(this.displays.findIndex(x => x._id == id), 1);
        });
    }
    onSelectDisplay(index) {
        this.selectedDisplay = this.displays[index];
        this.selectedDisplayID = this.selectedDisplay._id;
    }
    hideDisplaySelectClicked() {
        if (this.hideDisplaySelect) {
            this.hideDisplaySelect = false;
            this.displayColClass = "col-9";
        }
        else {
            this.hideDisplaySelect = true;
            this.displayColClass = "col-10";
        }
    }
    reload() {
        this.socket.emit("getDisplays");
    }
    ngOnDestroy() {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.socket.disconnect();
    }
}
MonitorComponent.ɵfac = function MonitorComponent_Factory(t) { return new (t || MonitorComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_Services_app_config_service__WEBPACK_IMPORTED_MODULE_3__["AppConfigService"])); };
MonitorComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: MonitorComponent, selectors: [["app-monitor"]], decls: 6, vars: 6, consts: [[1, "row"], ["class", "col-1", 4, "ngIf"], ["class", "col-3", 4, "ngIf"], [3, "display"], [1, "col-1"], ["mat-button", "", "type", "button", 1, "hideButton", 3, "click"], ["aria-label", "Aufklappen"], [1, "col-3"], [1, "outlineBorder"], ["mat-button", "", "type", "button", 1, "hideButton", "col-1", 2, "margin-left", "20px", 3, "click"], ["aria-label", "Neuladen"], [1, "header", "col"], ["mat-button", "", "type", "button", 1, "hideButton", "col-1", 3, "click"], ["aria-label", "Zuklappen"], [1, "group"], [3, "multiple"], [3, "selected", "click", 4, "ngFor", "ngForOf"], [3, "selected", "click"]], template: function MonitorComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, MonitorComponent_div_2_Template, 4, 0, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, MonitorComponent_div_3_Template, 14, 2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "app-display", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.hideDisplaySelect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.hideDisplaySelect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx.displayColClass);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("display", ctx.selectedDisplay);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], _shared_modules_display_display_component__WEBPACK_IMPORTED_MODULE_5__["DisplayComponent"], _angular_material_button__WEBPACK_IMPORTED_MODULE_6__["MatButton"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__["MatIcon"], _angular_material_list__WEBPACK_IMPORTED_MODULE_8__["MatSelectionList"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"], _angular_material_list__WEBPACK_IMPORTED_MODULE_8__["MatListOption"]], styles: [".hideButton[_ngcontent-%COMP%] {\n  background: #3F51B5;\n  color: white;\n  width: 20px;\n  height: 36px;\n  margin: 2px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXG1vbml0b3IuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUFDSiIsImZpbGUiOiJtb25pdG9yLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhpZGVCdXR0b24ge1xyXG4gICAgYmFja2dyb3VuZDogIzNGNTFCNTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgaGVpZ2h0OiAzNnB4O1xyXG4gICAgbWFyZ2luOiAycHg7XHJcbn0iXX0= */", "mat-selection-list[_ngcontent-%COMP%] {\n  max-height: 80vh;\n  overflow: auto;\n}\n\nmat-list-option[_ngcontent-%COMP%] {\n  border: solid 1px #CED4DA;\n}\n\nmat-list-option[_ngcontent-%COMP%]:hover {\n  background-color: #8898f7;\n}\n\n.mat-list-item.mat-list-single-selected-option[_ngcontent-%COMP%] {\n  background: #3F51B5;\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxsaXN0LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLHlCQUFBO0FBQ0o7O0FBR0E7RUFDSSx5QkFBQTtBQUFKOztBQUdBO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0FBQUoiLCJmaWxlIjoibGlzdC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibWF0LXNlbGVjdGlvbi1saXN0e1xyXG4gICAgbWF4LWhlaWdodDogODB2aDtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gIH1cclxuXHJcbm1hdC1saXN0LW9wdGlvbiB7XHJcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjQ0VENERBO1xyXG4gICAgXHJcbn1cclxuXHJcbm1hdC1saXN0LW9wdGlvbjpob3ZlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM4ODk4Zjc7XHJcbn1cclxuXHJcbi5tYXQtbGlzdC1pdGVtLm1hdC1saXN0LXNpbmdsZS1zZWxlY3RlZC1vcHRpb24ge1xyXG4gICAgYmFja2dyb3VuZDogIzNGNTFCNTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcblxyXG5cclxuIl19 */", ".col[_ngcontent-%COMP%] {\n  height: 100hv;\n}\n\n.row[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.subsectionHeader[_ngcontent-%COMP%] {\n  font-size: 19px;\n}\n\n.group[_ngcontent-%COMP%] {\n  margin-bottom: 7px;\n}\n\n.header[_ngcontent-%COMP%] {\n  text-decoration: underline;\n  text-align: center;\n  font-size: 24px;\n}\n\n.center[_ngcontent-%COMP%] {\n  width: 100%;\n  display: block;\n  text-align: center;\n}\n\nbutton[_ngcontent-%COMP%] {\n  margin-right: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxsYXlvdXQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7QUFDSiIsImZpbGUiOiJsYXlvdXQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb2wge1xyXG4gICAgaGVpZ2h0OiAxMDBodjtcclxufVxyXG5cclxuLnJvd3tcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uc3Vic2VjdGlvbkhlYWRlcntcclxuICAgIGZvbnQtc2l6ZTogMTlweCA7XHJcbn1cclxuXHJcbi5ncm91cHtcclxuICAgIG1hcmdpbi1ib3R0b206IDdweDtcclxufVxyXG5cclxuLmhlYWRlcntcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAyNHB4O1xyXG59XHJcblxyXG4uY2VudGVye1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuYnV0dG9ue1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn0iXX0= */", ".outlineBorder[_ngcontent-%COMP%] {\n  margin: 2px;\n  border: solid 1px #CED4DA;\n  padding: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxib3guc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7QUFDSiIsImZpbGUiOiJib3guc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5vdXRsaW5lQm9yZGVye1xyXG4gICAgbWFyZ2luOiAycHg7XHJcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjQ0VENERBO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG59XHJcbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MonitorComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-monitor',
                templateUrl: './monitor.component.html',
                styleUrls: [
                    './monitor.component.scss',
                    '../styles/global/list.scss',
                    '../styles/global/layout.scss',
                    '../styles/global/box.scss'
                ]
            }]
    }], function () { return [{ type: _Services_app_config_service__WEBPACK_IMPORTED_MODULE_3__["AppConfigService"] }]; }, null); })();


/***/ }),

/***/ "vIyT":
/*!***********************************************************!*\
  !*** ./src/app/layout-designer/layout-designer.module.ts ***!
  \***********************************************************/
/*! exports provided: LayoutDesignerModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutDesignerModule", function() { return LayoutDesignerModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _layout_designer_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./layout-designer.component */ "4wHx");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/list */ "MutI");
/* harmony import */ var _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared-modules/display/display.module */ "Z/RF");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/input */ "qFsG");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "5+WD");










const routes = [
    { path: 'Layoutdesigner', component: _layout_designer_component__WEBPACK_IMPORTED_MODULE_2__["LayoutDesignerComponent"] }
];
class LayoutDesignerModule {
}
LayoutDesignerModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: LayoutDesignerModule });
LayoutDesignerModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function LayoutDesignerModule_Factory(t) { return new (t || LayoutDesignerModule)(); }, imports: [[
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _angular_material_list__WEBPACK_IMPORTED_MODULE_4__["MatListModule"],
            _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_5__["DisplayModule"],
            _angular_material_input__WEBPACK_IMPORTED_MODULE_6__["MatInputModule"],
            _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_7__["DragDropModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](LayoutDesignerModule, { declarations: [_layout_designer_component__WEBPACK_IMPORTED_MODULE_2__["LayoutDesignerComponent"]], imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _angular_material_list__WEBPACK_IMPORTED_MODULE_4__["MatListModule"],
        _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_5__["DisplayModule"],
        _angular_material_input__WEBPACK_IMPORTED_MODULE_6__["MatInputModule"],
        _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_7__["DragDropModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LayoutDesignerModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [
                    _layout_designer_component__WEBPACK_IMPORTED_MODULE_2__["LayoutDesignerComponent"],
                ],
                imports: [
                    _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    _angular_material_list__WEBPACK_IMPORTED_MODULE_4__["MatListModule"],
                    _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_5__["DisplayModule"],
                    _angular_material_input__WEBPACK_IMPORTED_MODULE_6__["MatInputModule"],
                    _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_7__["DragDropModule"]
                ]
            }]
    }], null, null); })();


/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _configuration_configuration_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./configuration/configuration.module */ "xjgD");
/* harmony import */ var _monitor_monitor_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./monitor/monitor.module */ "PkOl");
/* harmony import */ var _working_working_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./working/working.module */ "C98j");







const routes = [];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes),
            _monitor_monitor_module__WEBPACK_IMPORTED_MODULE_3__["MonitorModule"],
            _working_working_module__WEBPACK_IMPORTED_MODULE_4__["WorkingModule"],
            _configuration_configuration_module__WEBPACK_IMPORTED_MODULE_2__["ConfigurationModule"]
        ], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"], _monitor_monitor_module__WEBPACK_IMPORTED_MODULE_3__["MonitorModule"],
        _working_working_module__WEBPACK_IMPORTED_MODULE_4__["WorkingModule"],
        _configuration_configuration_module__WEBPACK_IMPORTED_MODULE_2__["ConfigurationModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [
                    _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes),
                    _monitor_monitor_module__WEBPACK_IMPORTED_MODULE_3__["MonitorModule"],
                    _working_working_module__WEBPACK_IMPORTED_MODULE_4__["WorkingModule"],
                    _configuration_configuration_module__WEBPACK_IMPORTED_MODULE_2__["ConfigurationModule"]
                ],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "wUxT":
/*!**************************************************!*\
  !*** ./src/app/Models/DisplayInterfaces.Enum.ts ***!
  \**************************************************/
/*! exports provided: DisplayInterfaces */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisplayInterfaces", function() { return DisplayInterfaces; });
var DisplayInterfaces;
(function (DisplayInterfaces) {
    // Network = 1,
    // RS232 = 2
    DisplayInterfaces["Network"] = "Network";
    DisplayInterfaces["RS232"] = "RS232";
})(DisplayInterfaces || (DisplayInterfaces = {}));


/***/ }),

/***/ "xdQH":
/*!*****************************************************!*\
  !*** ./src/app/working/working-facade.component.ts ***!
  \*****************************************************/
/*! exports provided: WorkingFacadeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WorkingFacadeComponent", function() { return WorkingFacadeComponent; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_Models_Display_Model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Models/Display.Model */ "QX5R");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "wd/R");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var src_app_Services_working_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Services/working.service */ "VcEb");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/list */ "MutI");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/button */ "bTqV");
/* harmony import */ var _shared_modules_display_display_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared-modules/display/display.component */ "zzkD");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/icon */ "NFeN");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/form-field */ "kmnG");












function WorkingFacadeComponent_mat_list_option_8_Template(rf, ctx) { if (rf & 1) {
    const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "mat-list-option", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function WorkingFacadeComponent_mat_list_option_8_Template_mat_list_option_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r7); const i_r5 = ctx.index; const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r6.onSelectDisplay(i_r5); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const display_r4 = ctx.$implicit;
    const i_r5 = ctx.index;
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("selected", i_r5 == ctx_r0.selectedDisplayIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](display_r4.name);
} }
function WorkingFacadeComponent_mat_list_option_15_Template(rf, ctx) { if (rf & 1) {
    const _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "mat-list-option", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function WorkingFacadeComponent_mat_list_option_15_Template_mat_list_option_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r11); const i_r9 = ctx.index; const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r10.onSelectInterface(i_r9); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const interface_r8 = ctx.$implicit;
    const i_r9 = ctx.index;
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("selected", i_r9 == ctx_r1.selectedInterfaceIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](interface_r8);
} }
function WorkingFacadeComponent_button_30_Template(rf, ctx) { if (rf & 1) {
    const _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function WorkingFacadeComponent_button_30_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r13); const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r12.reportAccident(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, " Melde Unfall ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "mat-icon", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "warning");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function WorkingFacadeComponent_div_35_span_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Zeit: Die Zeit muss in folgendem Format angegeben werden: 'HH:MM'");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function WorkingFacadeComponent_div_35_span_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Datum: Das Datum muss in folgendem Format angegeben werden: 'TT.MM.JJ'");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function WorkingFacadeComponent_div_35_Template(rf, ctx) { if (rf & 1) {
    const _r17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "mat-label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Anzeigenzeit:");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "input", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function WorkingFacadeComponent_div_35_Template_input_change_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r17); const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r16.setTime($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8, " - ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "input", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function WorkingFacadeComponent_div_35_Template_input_change_10_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r17); const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r18.setDate($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, WorkingFacadeComponent_div_35_span_11_Template, 2, 0, "span", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](12, WorkingFacadeComponent_div_35_span_12_Template, 2, 0, "span", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "button", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function WorkingFacadeComponent_div_35_Template_button_click_14_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r17); const ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r19.sendSystemTime(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15, " Sende Systemzeit ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "mat-icon", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17, "access_time");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "button", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function WorkingFacadeComponent_div_35_Template_button_click_18_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r17); const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r20.saveTime(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](19, " Speicher Zeit ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "mat-icon", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](21, "save");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("value", ctx_r3.selectedDisplay.time ? ctx_r3.selectedDisplay.time : "00:00");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("value", ctx_r3.selectedDisplay.date ? ctx_r3.selectedDisplay.date : "00.00.00");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r3.isTimeValid);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r3.isDateValid);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r3.isTimeValid && !ctx_r3.isDateValid);
} }
class WorkingFacadeComponent {
    constructor(workingService) {
        this.workingService = workingService;
        this.selectedDisplay = new src_app_Models_Display_Model__WEBPACK_IMPORTED_MODULE_2__["IDisplay"]();
        this.subscriptions = [];
        this.displays = [];
        this.selectedDisplayIndex = -1;
        this.selectedInterfaceIndex = -1;
        this.logText = "";
        this.isTimeValid = false;
        this.isDateValid = false;
    }
    ngOnInit() {
        this.workingService.initialize();
        setTimeout(() => {
            this.subscriptions.push(this.workingService.displaysChanged.subscribe(displays => {
                this.displays = [...displays];
                if (this.selectedDisplayIndex !== -1) {
                    this.selectedDisplay = this.displays[this.selectedDisplayIndex];
                    this.checkDateAndTimeManually();
                }
            }));
            this.subscriptions.push(this.workingService.LogSubject.subscribe(log => {
                this.logText += log;
            }));
            setTimeout(() => {
                this.workingService.triggerDisplaysChanged();
                setTimeout(() => {
                    if (this.displays.length != 0) {
                        {
                            this.onSelectDisplay(0);
                            if (this.displays[this.selectedDisplayIndex].interfaces.length != 0) {
                                this.onSelectInterface(0);
                            }
                        }
                    }
                }, 500);
            }, 500);
        }, 500);
    }
    onSelectDisplay(index) {
        this.workingService.selectedDisplaySubject.next(index);
        this.selectedDisplay = this.displays[index];
        this.selectedDisplayIndex = index;
        this.checkDateAndTimeManually();
    }
    onSelectInterface(index) {
        this.selectedInterfaceIndex = index;
        if (this.selectedDisplay.useAccidentsLastYear || this.selectedDisplay.useAccidentsThisYear ||
            this.selectedDisplay.useDaysWithoutAccidents || this.selectedDisplay.useDaysWithoutAccidentsHighscore) {
            this.workingService.interfaceSelected(this.selectedInterfaceIndex);
            this.checkDateAndTimeManually();
        }
    }
    ngOnDestroy() {
        this.subscriptions.forEach(sub => {
            sub.unsubscribe();
        });
    }
    onDisplayChanged(event) {
        this.selectedDisplay = event;
    }
    setTime(event) {
        if (event !== undefined && event !== null) {
            if (event.target !== undefined && event.target !== null) {
                if (event.target.value !== undefined || event.target.value !== null) {
                    if (event.target.value.match(/^([0-2][0-3]:[0-5][0-9]|[0-1][0-9]:[0-5][0-9])$/g)) {
                        this.isTimeValid = true;
                        this.selectedDisplay.time = event.target.value;
                    }
                    else {
                        this.isTimeValid = false;
                    }
                }
            }
        }
    }
    checkDateAndTimeManually() {
        var _a, _b;
        if ((_b = (_a = this.selectedDisplay) === null || _a === void 0 ? void 0 : _a.time) === null || _b === void 0 ? void 0 : _b.match(/^([0-2][0-3]:[0-5][0-9]|[0-1][0-9]:[0-5][0-9])$/g)) {
            this.isTimeValid = true;
        }
        else {
            this.isTimeValid = false;
        }
        if (moment__WEBPACK_IMPORTED_MODULE_3__(this.selectedDisplay.date, "DD.MM.YY", true).isValid()) {
            this.isDateValid = true;
        }
        else {
            this.isDateValid = false;
        }
    }
    setDate(event) {
        if (event !== undefined && event !== null) {
            if (event.target !== undefined && event.target !== null) {
                if (event.target.value !== undefined || event.target.value !== null) {
                    if (moment__WEBPACK_IMPORTED_MODULE_3__(event.target.value, "DD.MM.YY", true).isValid()) {
                        this.selectedDisplay.date = event.target.value;
                        this.isDateValid = true;
                    }
                    else {
                        this.isDateValid = false;
                    }
                }
            }
        }
    }
    reportAccident() {
        this.workingService.reportAccident(this.selectedDisplay);
    }
    sendData() {
        this.workingService.sendAccidentData(this.selectedDisplay);
    }
    sendSystemTime() {
        let dateTimeNow = new Date();
        this.selectedDisplay.date = Object(_angular_common__WEBPACK_IMPORTED_MODULE_0__["formatDate"])(dateTimeNow, 'dd.MM.yy', 'de').toString();
        this.selectedDisplay.time = Object(_angular_common__WEBPACK_IMPORTED_MODULE_0__["formatDate"])(dateTimeNow, 'HH:mm', 'de').toString();
        this.saveTime();
    }
    saveTime() {
        this.workingService.sendDateTimeData(this.selectedDisplay);
    }
    clearLog() {
        this.logText = "";
    }
}
WorkingFacadeComponent.ɵfac = function WorkingFacadeComponent_Factory(t) { return new (t || WorkingFacadeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_Services_working_service__WEBPACK_IMPORTED_MODULE_4__["WorkingService"])); };
WorkingFacadeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: WorkingFacadeComponent, selectors: [["app-working-facade"]], decls: 36, vars: 8, consts: [[1, "row"], [1, "col-3"], [1, "outlineBorder"], [1, "header"], [1, "group"], [3, "multiple"], [3, "selected", "click", 4, "ngFor", "ngForOf"], [1, "outlineBorder", 2, "max-height", "62vh"], [2, "max-height", "53vh", 3, "multiple"], [1, "col-12"], ["rows", "9", "cols", "200", "disabled", "true", 1, "col-12"], [1, "col-12", "center"], ["mat-raised-button", "", "color", "primary", "type", "button", 3, "click"], [1, "col-6"], [3, "display", "displayChanged"], [1, "group", "center", 2, "margin-top", "5px"], ["mat-raised-button", "", "color", "primary", "type", "button", 3, "click", 4, "ngIf"], ["aria-label", "Anzeige l\u00F6schen"], ["class", "outlineBorder row", 4, "ngIf"], [3, "selected", "click"], ["aria-label", "Neue Anzeige"], [1, "outlineBorder", "row"], [1, "row", 2, "margin-bottom", "5px"], [1, "col-5", "text"], [1, "text"], ["matInput", "", "type", "text", 1, "form-control", 3, "value", "change"], [1, "col-1", "text"], [4, "ngIf"], [1, "center"], ["mat-raised-button", "", "color", "primary", "type", "button", 3, "disabled", "click"]], template: function WorkingFacadeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "Anzeigen:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "mat-selection-list", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, WorkingFacadeComponent_mat_list_option_8_Template, 2, 2, "mat-list-option", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12, "Schnittstellen:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "mat-selection-list", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](15, WorkingFacadeComponent_mat_list_option_15_Template, 2, 2, "mat-list-option", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "h2", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18, "Log:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "textarea", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](22);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function WorkingFacadeComponent_Template_button_click_24_listener() { return ctx.clearLog(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](25, "L\u00F6sche Log");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "app-display", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("displayChanged", function WorkingFacadeComponent_Template_app_display_displayChanged_28_listener($event) { return ctx.onDisplayChanged($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](30, WorkingFacadeComponent_button_30_Template, 4, 0, "button", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](31, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function WorkingFacadeComponent_Template_button_click_31_listener() { return ctx.sendData(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](32, " Sende Daten ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](33, "mat-icon", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](34, "send");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](35, WorkingFacadeComponent_div_35_Template, 22, 5, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("multiple", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.displays);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("multiple", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.selectedDisplay.interfaces);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.logText);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("display", ctx.selectedDisplay);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.selectedDisplay.useAccidentsLastYear || ctx.selectedDisplay.useAccidentsThisYear || ctx.selectedDisplay.useDaysWithoutAccidents || ctx.selectedDisplay.useDaysWithoutAccidentsHighscore);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.selectedDisplay.useAccidentsLastYear || ctx.selectedDisplay.useAccidentsThisYear || ctx.selectedDisplay.useDaysWithoutAccidents || ctx.selectedDisplay.useDaysWithoutAccidentsHighscore);
    } }, directives: [_angular_material_list__WEBPACK_IMPORTED_MODULE_5__["MatSelectionList"], _angular_common__WEBPACK_IMPORTED_MODULE_0__["NgForOf"], _angular_material_button__WEBPACK_IMPORTED_MODULE_6__["MatButton"], _shared_modules_display_display_component__WEBPACK_IMPORTED_MODULE_7__["DisplayComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_0__["NgIf"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__["MatIcon"], _angular_material_list__WEBPACK_IMPORTED_MODULE_5__["MatListOption"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__["MatLabel"]], styles: ["textarea[_ngcontent-%COMP%] {\n  resize: none;\n  max-height: 200px;\n  margin-top: 5px;\n  border-color: #CED4DA;\n}\n\n.button[_ngcontent-%COMP%] {\n  width: 100%;\n  margin-top: 5px;\n}\n\n.column[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n  height: 92vh;\n}\n\n.text[_ngcontent-%COMP%] {\n  margin-top: 5px;\n  height: auto;\n  text-align: center;\n  font-size: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHdvcmtpbmctZmFjYWRlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLHFCQUFBO0FBQ0o7O0FBR0E7RUFDSSxXQUFBO0VBQ0EsZUFBQTtBQUFKOztBQUdBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsOEJBQUE7RUFDQSxZQUFBO0FBQUo7O0FBR0E7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQUFKIiwiZmlsZSI6IndvcmtpbmctZmFjYWRlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGV4dGFyZWEge1xyXG4gICAgcmVzaXplOiBub25lO1xyXG4gICAgbWF4LWhlaWdodDogMjAwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICBib3JkZXItY29sb3I6ICNDRUQ0REE7XHJcbiAgICBcclxufVxyXG5cclxuLmJ1dHRvbiB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbi10b3A6IDVweDtcclxufVxyXG5cclxuLmNvbHVtbiB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIGhlaWdodDogOTJ2aDtcclxufVxyXG5cclxuLnRleHQge1xyXG4gICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG59Il19 */", "mat-selection-list[_ngcontent-%COMP%] {\n  max-height: 80vh;\n  overflow: auto;\n}\n\nmat-list-option[_ngcontent-%COMP%] {\n  border: solid 1px #CED4DA;\n}\n\nmat-list-option[_ngcontent-%COMP%]:hover {\n  background-color: #8898f7;\n}\n\n.mat-list-item.mat-list-single-selected-option[_ngcontent-%COMP%] {\n  background: #3F51B5;\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxsaXN0LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLHlCQUFBO0FBQ0o7O0FBR0E7RUFDSSx5QkFBQTtBQUFKOztBQUdBO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0FBQUoiLCJmaWxlIjoibGlzdC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibWF0LXNlbGVjdGlvbi1saXN0e1xyXG4gICAgbWF4LWhlaWdodDogODB2aDtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gIH1cclxuXHJcbm1hdC1saXN0LW9wdGlvbiB7XHJcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjQ0VENERBO1xyXG4gICAgXHJcbn1cclxuXHJcbm1hdC1saXN0LW9wdGlvbjpob3ZlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM4ODk4Zjc7XHJcbn1cclxuXHJcbi5tYXQtbGlzdC1pdGVtLm1hdC1saXN0LXNpbmdsZS1zZWxlY3RlZC1vcHRpb24ge1xyXG4gICAgYmFja2dyb3VuZDogIzNGNTFCNTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcblxyXG5cclxuIl19 */", ".col[_ngcontent-%COMP%] {\n  height: 100hv;\n}\n\n.row[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.subsectionHeader[_ngcontent-%COMP%] {\n  font-size: 19px;\n}\n\n.group[_ngcontent-%COMP%] {\n  margin-bottom: 7px;\n}\n\n.header[_ngcontent-%COMP%] {\n  text-decoration: underline;\n  text-align: center;\n  font-size: 24px;\n}\n\n.center[_ngcontent-%COMP%] {\n  width: 100%;\n  display: block;\n  text-align: center;\n}\n\nbutton[_ngcontent-%COMP%] {\n  margin-right: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxsYXlvdXQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7QUFDSiIsImZpbGUiOiJsYXlvdXQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb2wge1xyXG4gICAgaGVpZ2h0OiAxMDBodjtcclxufVxyXG5cclxuLnJvd3tcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uc3Vic2VjdGlvbkhlYWRlcntcclxuICAgIGZvbnQtc2l6ZTogMTlweCA7XHJcbn1cclxuXHJcbi5ncm91cHtcclxuICAgIG1hcmdpbi1ib3R0b206IDdweDtcclxufVxyXG5cclxuLmhlYWRlcntcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAyNHB4O1xyXG59XHJcblxyXG4uY2VudGVye1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuYnV0dG9ue1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn0iXX0= */", ".outlineBorder[_ngcontent-%COMP%] {\n  margin: 2px;\n  border: solid 1px #CED4DA;\n  padding: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxib3guc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7QUFDSiIsImZpbGUiOiJib3guc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5vdXRsaW5lQm9yZGVye1xyXG4gICAgbWFyZ2luOiAycHg7XHJcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjQ0VENERBO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG59XHJcbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](WorkingFacadeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
                selector: 'app-working-facade',
                templateUrl: './working-facade.component.html',
                styleUrls: [
                    './working-facade.component.scss',
                    '../styles/global/list.scss',
                    '../styles/global/layout.scss',
                    '../styles/global/box.scss'
                ]
            }]
    }], function () { return [{ type: src_app_Services_working_service__WEBPACK_IMPORTED_MODULE_4__["WorkingService"] }]; }, null); })();


/***/ }),

/***/ "xjgD":
/*!*******************************************************!*\
  !*** ./src/app/configuration/configuration.module.ts ***!
  \*******************************************************/
/*! exports provided: ConfigurationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigurationModule", function() { return ConfigurationModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/checkbox */ "bSwM");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/button */ "bTqV");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/card */ "Wp6s");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/form-field */ "kmnG");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/icon */ "NFeN");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/input */ "qFsG");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/list */ "MutI");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/radio */ "QibW");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/toolbar */ "/t3+");
/* harmony import */ var _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../shared-modules/display/display.module */ "Z/RF");
/* harmony import */ var _configuration_facade_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./configuration-facade.component */ "Aq2v");
/* harmony import */ var _display_configuration_form_display_configuration_form_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./display-configuration-form/display-configuration-form.component */ "DrwV");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/router */ "tyNb");


















const routes = [
    { path: 'Konfiguration', component: _configuration_facade_component__WEBPACK_IMPORTED_MODULE_13__["ConfigurationFacadeComponent"] }
];
class ConfigurationModule {
}
ConfigurationModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: ConfigurationModule });
ConfigurationModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function ConfigurationModule_Factory(t) { return new (t || ConfigurationModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_12__["DisplayModule"],
            _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_3__["MatCheckboxModule"],
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_6__["MatFormFieldModule"],
            _angular_material_input__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"],
            _angular_material_radio__WEBPACK_IMPORTED_MODULE_10__["MatRadioModule"],
            _angular_material_list__WEBPACK_IMPORTED_MODULE_9__["MatListModule"],
            _angular_material_button__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
            _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_11__["MatToolbarModule"],
            _angular_material_card__WEBPACK_IMPORTED_MODULE_5__["MatCardModule"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_15__["RouterModule"].forChild(routes)
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](ConfigurationModule, { declarations: [_configuration_facade_component__WEBPACK_IMPORTED_MODULE_13__["ConfigurationFacadeComponent"],
        _display_configuration_form_display_configuration_form_component__WEBPACK_IMPORTED_MODULE_14__["DisplayConfigurationFormComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_12__["DisplayModule"],
        _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_3__["MatCheckboxModule"],
        _angular_material_form_field__WEBPACK_IMPORTED_MODULE_6__["MatFormFieldModule"],
        _angular_material_input__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"],
        _angular_material_radio__WEBPACK_IMPORTED_MODULE_10__["MatRadioModule"],
        _angular_material_list__WEBPACK_IMPORTED_MODULE_9__["MatListModule"],
        _angular_material_button__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
        _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_11__["MatToolbarModule"],
        _angular_material_card__WEBPACK_IMPORTED_MODULE_5__["MatCardModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_15__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ConfigurationModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [
                    _configuration_facade_component__WEBPACK_IMPORTED_MODULE_13__["ConfigurationFacadeComponent"],
                    _display_configuration_form_display_configuration_form_component__WEBPACK_IMPORTED_MODULE_14__["DisplayConfigurationFormComponent"]
                ],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    _shared_modules_display_display_module__WEBPACK_IMPORTED_MODULE_12__["DisplayModule"],
                    _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_3__["MatCheckboxModule"],
                    _angular_material_form_field__WEBPACK_IMPORTED_MODULE_6__["MatFormFieldModule"],
                    _angular_material_input__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"],
                    _angular_material_radio__WEBPACK_IMPORTED_MODULE_10__["MatRadioModule"],
                    _angular_material_list__WEBPACK_IMPORTED_MODULE_9__["MatListModule"],
                    _angular_material_button__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                    _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_11__["MatToolbarModule"],
                    _angular_material_card__WEBPACK_IMPORTED_MODULE_5__["MatCardModule"],
                    _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                    _angular_router__WEBPACK_IMPORTED_MODULE_15__["RouterModule"].forChild(routes)
                ]
            }]
    }], null, null); })();


/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "AytR");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ }),

/***/ "zzkD":
/*!*************************************************************!*\
  !*** ./src/app/shared-modules/display/display.component.ts ***!
  \*************************************************************/
/*! exports provided: DisplayComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisplayComponent", function() { return DisplayComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_Models_Display_Model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/Models/Display.Model */ "QX5R");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/form-field */ "kmnG");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _accidents_this_year_accidents_this_year_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./accidents-this-year/accidents-this-year.component */ "VEDO");
/* harmony import */ var _accidents_last_year_accidents_last_year_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./accidents-last-year/accidents-last-year.component */ "jyKa");
/* harmony import */ var _days_without_accidents_days_without_accidents_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./days-without-accidents/days-without-accidents.component */ "+QUk");
/* harmony import */ var _days_without_accidents_highscore_days_without_accidents_highscore_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./days-without-accidents-highscore/days-without-accidents-highscore.component */ "fnaR");
/* harmony import */ var _gtx_text_gtx_text_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./gtx-text/gtx-text.component */ "r2rP");










function DisplayComponent_app_accidents_this_year_4_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "app-accidents-this-year", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("accidentsChange", function DisplayComponent_app_accidents_this_year_4_Template_app_accidents_this_year_accidentsChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.updateAccidentsThisYear($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("accidents", ctx_r0.display.displayContent.accidentsThisYear)("manuelModus", ctx_r0.display.manualModus);
} }
function DisplayComponent_app_accidents_last_year_5_Template(rf, ctx) { if (rf & 1) {
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "app-accidents-last-year", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("accidentsChange", function DisplayComponent_app_accidents_last_year_5_Template_app_accidents_last_year_accidentsChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r7.updateAccidentsLastYear($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("accidents", ctx_r1.display.displayContent.accidentsLastYear)("manuelModus", ctx_r1.display.manualModus);
} }
function DisplayComponent_app_days_without_accidents_6_Template(rf, ctx) { if (rf & 1) {
    const _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "app-days-without-accidents", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("accidentsChange", function DisplayComponent_app_days_without_accidents_6_Template_app_days_without_accidents_accidentsChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r10); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r9.updateDaysWithoutAccidents($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("accidents", ctx_r2.display.displayContent.daysWithoutAccidents)("manuelModus", ctx_r2.display.manualModus);
} }
function DisplayComponent_app_days_without_accidents_highscore_7_Template(rf, ctx) { if (rf & 1) {
    const _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "app-days-without-accidents-highscore", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("accidentsChange", function DisplayComponent_app_days_without_accidents_highscore_7_Template_app_days_without_accidents_highscore_accidentsChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r12); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r11.updateDaysWithoutAccidentsHighscore($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("accidents", ctx_r3.display.displayContent.daysWithoutAccidentsHighscore)("manuelModus", ctx_r3.display.manualModus);
} }
function DisplayComponent_app_gtx_text_8_Template(rf, ctx) { if (rf & 1) {
    const _r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "app-gtx-text", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("row1Change", function DisplayComponent_app_gtx_text_8_Template_app_gtx_text_row1Change_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r14); const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r13.updateRow1($event); })("row2Change", function DisplayComponent_app_gtx_text_8_Template_app_gtx_text_row2Change_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r14); const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r15.updateRow2($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rowsCount", ctx_r4.display.gtxRows)("characterCount", ctx_r4.display.gtxTextLength)("row1", ctx_r4.display.displayContent.gtxTextFirstRow)("row2", ctx_r4.display.displayContent.gtxTextSecoundRow);
} }
class DisplayComponent {
    constructor() {
        this.display = new src_app_Models_Display_Model__WEBPACK_IMPORTED_MODULE_1__["IDisplay"]();
        this.displayChanged = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    updateAccidentsThisYear(event) {
        this.display.displayContent.accidentsThisYear = event;
        this.displayChanged.emit(this.display);
    }
    updateAccidentsLastYear(event) {
        this.display.displayContent.accidentsLastYear = event;
        this.displayChanged.emit(this.display);
    }
    updateDaysWithoutAccidents(event) {
        this.display.displayContent.daysWithoutAccidents = event;
        this.displayChanged.emit(this.display);
    }
    updateDaysWithoutAccidentsHighscore(event) {
        this.display.displayContent.daysWithoutAccidentsHighscore = event;
        this.displayChanged.emit(this.display);
    }
    updateRow1(event) {
        this.display.displayContent.gtxTextFirstRow = event;
        this.displayChanged.emit(this.display);
    }
    updateRow2(event) {
        this.display.displayContent.gtxTextSecoundRow = event;
        this.displayChanged.emit(this.display);
    }
}
DisplayComponent.ɵfac = function DisplayComponent_Factory(t) { return new (t || DisplayComponent)(); };
DisplayComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: DisplayComponent, selectors: [["app-display"]], inputs: { display: "display" }, outputs: { displayChanged: "displayChanged" }, decls: 9, vars: 6, consts: [[1, "display"], [1, "row"], [1, "displayText", "displayHeader"], [3, "accidents", "manuelModus", "accidentsChange", 4, "ngIf"], [3, "rowsCount", "characterCount", "row1", "row2", "row1Change", "row2Change", 4, "ngIf"], [3, "accidents", "manuelModus", "accidentsChange"], [3, "rowsCount", "characterCount", "row1", "row2", "row1Change", "row2Change"]], template: function DisplayComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-label", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, DisplayComponent_app_accidents_this_year_4_Template, 1, 2, "app-accidents-this-year", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, DisplayComponent_app_accidents_last_year_5_Template, 1, 2, "app-accidents-last-year", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, DisplayComponent_app_days_without_accidents_6_Template, 1, 2, "app-days-without-accidents", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, DisplayComponent_app_days_without_accidents_highscore_7_Template, 1, 2, "app-days-without-accidents-highscore", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, DisplayComponent_app_gtx_text_8_Template, 1, 4, "app-gtx-text", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.display.name);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.display.useAccidentsThisYear);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.display.useAccidentsLastYear);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.display.useDaysWithoutAccidents);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.display.useDaysWithoutAccidentsHighscore);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.display.useGtxText);
    } }, directives: [_angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatLabel"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _accidents_this_year_accidents_this_year_component__WEBPACK_IMPORTED_MODULE_4__["AccidentsThisYearComponent"], _accidents_last_year_accidents_last_year_component__WEBPACK_IMPORTED_MODULE_5__["AccidentsLastYearComponent"], _days_without_accidents_days_without_accidents_component__WEBPACK_IMPORTED_MODULE_6__["DaysWithoutAccidentsComponent"], _days_without_accidents_highscore_days_without_accidents_highscore_component__WEBPACK_IMPORTED_MODULE_7__["DaysWithoutAccidentsHighscoreComponent"], _gtx_text_gtx_text_component__WEBPACK_IMPORTED_MODULE_8__["GTXTextComponent"]], styles: [".row[_ngcontent-%COMP%] {\n  margin: 2px;\n}\n\n.display[_ngcontent-%COMP%] {\n  background-color: black;\n  border-color: #a7a7a7;\n  border-width: 5px;\n  border-style: solid;\n  height: auto;\n  min-height: 50px;\n  padding: 3px;\n}\n\n.displayText[_ngcontent-%COMP%] {\n  background-color: black;\n  color: white;\n  align-items: center;\n}\n\n.displayHeader[_ngcontent-%COMP%] {\n  text-decoration: underline;\n  text-align: center;\n  font-size: 35px;\n  height: 60px;\n  width: 99%;\n  display: grid;\n}\n\n.displayLabel[_ngcontent-%COMP%] {\n  font-size: 30px;\n  display: flex;\n}\n\n.displayInput[_ngcontent-%COMP%] {\n  background-color: black;\n  border-color: #a7a7a7;\n  border-width: 2px;\n  border-style: solid;\n  color: red;\n  padding: 4px;\n  height: 50;\n  font-size: 40px;\n  -webkit-appearance: none;\n  -moz-appearance: none;\n}\n\n.accident[_ngcontent-%COMP%] {\n  text-align: right;\n  font-family: \"numeric\";\n}\n\n.notification[_ngcontent-%COMP%] {\n  font-family: \"editundot\";\n  text-align: center;\n  margin: 2px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxkaXNwbGF5LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksMEJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7QUFDSjs7QUFHQTtFQUNJLGVBQUE7RUFDQSxhQUFBO0FBQUo7O0FBR0E7RUFDSSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFFQSx3QkFBQTtFQUNBLHFCQUFBO0FBREo7O0FBSUE7RUFDSSxpQkFBQTtFQUNBLHNCQUFBO0FBREo7O0FBSUE7RUFDSSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQURKIiwiZmlsZSI6ImRpc3BsYXkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucm93IHtcclxuICAgIG1hcmdpbjogMnB4O1xyXG59XHJcblxyXG4uZGlzcGxheSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICAgIGJvcmRlci1jb2xvcjogcmdiKDE2NywgMTY3LCAxNjcpO1xyXG4gICAgYm9yZGVyLXdpZHRoOiA1cHg7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgbWluLWhlaWdodDogNTBweDtcclxuICAgIHBhZGRpbmc6IDNweDtcclxufVxyXG5cclxuLmRpc3BsYXlUZXh0e1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4uZGlzcGxheUhlYWRlcntcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAzNXB4O1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgd2lkdGg6IDk5JTtcclxuICAgIGRpc3BsYXk6IGdyaWQ7XHJcbn1cclxuXHJcblxyXG4uZGlzcGxheUxhYmVse1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuLmRpc3BsYXlJbnB1dHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMTY3LCAxNjcsIDE2Nyk7XHJcbiAgICBib3JkZXItd2lkdGg6IDJweDtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgcGFkZGluZzogNHB4O1xyXG4gICAgaGVpZ2h0OiA1MDtcclxuICAgIGZvbnQtc2l6ZTogNDBweDtcclxuXHJcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6bm9uZTsgXHJcbiAgICAtbW96LWFwcGVhcmFuY2U6IG5vbmU7IFxyXG59XHJcblxyXG4uYWNjaWRlbnQge1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBmb250LWZhbWlseTogJ251bWVyaWMnO1xyXG59XHJcblxyXG4ubm90aWZpY2F0aW9uIHtcclxuICAgIGZvbnQtZmFtaWx5OiAnZWRpdHVuZG90JztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMnB4O1xyXG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DisplayComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-display',
                templateUrl: './display.component.html',
                styleUrls: [
                    './display.component.scss'
                ]
            }]
    }], function () { return []; }, { display: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], displayChanged: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }] }); })();


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map