import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutDesignerComponent } from './layout-designer.component';
import { RouterModule, Routes } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { DisplayModule } from '../shared-modules/display/display.module';
import { MatInputModule } from '@angular/material/input';
import { DragDropModule } from '@angular/cdk/drag-drop';

const routes: Routes = [
  { path: 'Layoutdesigner', component: LayoutDesignerComponent}
];

@NgModule({
  declarations: [
    LayoutDesignerComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MatListModule,
    DisplayModule,
    MatInputModule,
    DragDropModule
  ]
})
export class LayoutDesignerModule { }
