import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { IDisplay } from '../Models/Display.Model';
import { WorkingService } from '../Services/working.service';

@Component({
  selector: 'app-layout-designer',
  templateUrl: './layout-designer.component.html',
  styleUrls: [
    './layout-designer.component.scss',
  '../styles/global/list.scss',
  '../styles/global/layout.scss',
  '../styles/global/box.scss'
]
})
export class LayoutDesignerComponent implements OnInit, OnDestroy {
  selectedDisplay: IDisplay = new IDisplay();
  private subscriptions: Subscription[] = [];
  displays: IDisplay[] = [];
  selectedDisplayIndex: number = -1;
  selectedInterfaceIndex: number = -1;
  logText: string = "";
  isTimeValid: boolean = false;
  isDateValid: boolean = false;


  constructor(private workingService: WorkingService) { }

  ngOnInit(): void {
    this.workingService.initialize();

    setTimeout(() => {
      this.subscriptions.push(this.workingService.displaysChanged.subscribe(displays => {
        this.displays = [...displays];
        if (this.selectedDisplayIndex !== -1) {
          this.selectedDisplay = this.displays[this.selectedDisplayIndex];
        }
      }));

      this.subscriptions.push(this.workingService.LogSubject.subscribe(log => {
        this.logText += log;
      }))


      setTimeout(() => {
        this.workingService.triggerDisplaysChanged();

        setTimeout(() => {
          if (this.displays.length != 0) {
            {
              this.onSelectDisplay(0);
              if (this.displays[this.selectedDisplayIndex].interfaces.length != 0) {
                this.onSelectInterface(0);
              }
            }
          }
        }, 500);
      }, 500);
    }, 500);


    
  }

  onSelectDisplay(index: number) {
    this.workingService.selectedDisplaySubject.next(index);
    this.selectedDisplay = this.displays[index];
    this.selectedDisplayIndex = index;
  }

  onSelectInterface(index: number) {
    this.selectedInterfaceIndex = index;
    if (this.selectedDisplay.useAccidentsLastYear || this.selectedDisplay.useAccidentsThisYear ||
      this.selectedDisplay.useDaysWithoutAccidents || this.selectedDisplay.useDaysWithoutAccidentsHighscore) {
      this.workingService.interfaceSelected(this.selectedInterfaceIndex);
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    })
  }

  onDisplayChanged(event: IDisplay) {
    this.selectedDisplay = event;
  }

  setTime(event: any) {
    if (event !== undefined && event !== null) {
      if (event.target !== undefined && event.target !== null) {
        if (event.target.value !== undefined || event.target.value !== null) {
          if (event.target.value.match(/^([0-2][0-3]:[0-5][0-9]|[0-1][0-9]:[0-5][0-9])$/g)) {
            this.isTimeValid = true;
            this.selectedDisplay.time = event.target.value;
          } else {
            this.isTimeValid = false;
          }
        }
      }
    }
  }

  reportAccident() {
    this.workingService.reportAccident(this.selectedDisplay);
  }

  sendData() {
    this.workingService.sendAccidentData(this.selectedDisplay);
  }

  saveTime() {
    this.workingService.sendDateTimeData(this.selectedDisplay);
  }

  clearLog() {
    this.logText = "";
  }
}
