import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { IDisplay } from 'src/app/Models/Display.Model';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: [
    './display.component.scss'
  ]
})
export class DisplayComponent {
  subscription: Subscription;
  editedItemIndex: number;
  @Input() display: IDisplay = new IDisplay();
  @Output() displayChanged = new EventEmitter<IDisplay>();


  constructor() {
   }
  
   updateAccidentsThisYear(event:number){
    this.display.displayContent.accidentsThisYear = event; 
    this.displayChanged.emit(this.display);
   }

   updateAccidentsLastYear(event:number){
    this.display.displayContent.accidentsLastYear = event; 
    this.displayChanged.emit(this.display);
   }

   updateDaysWithoutAccidents(event:number){
    this.display.displayContent.daysWithoutAccidents = event; 
    this.displayChanged.emit(this.display);
   }

  updateDaysWithoutAccidentsHighscore(event: number){
    this.display.displayContent.daysWithoutAccidentsHighscore = event;
    this.displayChanged.emit(this.display);
  }

  updateRow1(event: string){
    this.display.displayContent.gtxTextFirstRow = event;
    this.displayChanged.emit(this.display);
  }

  updateRow2(event: string){
    this.display.displayContent.gtxTextSecoundRow = event;
    this.displayChanged.emit(this.display);
  }

}
