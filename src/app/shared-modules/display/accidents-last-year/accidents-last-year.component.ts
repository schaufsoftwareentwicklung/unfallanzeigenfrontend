import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-accidents-last-year',
  templateUrl: './accidents-last-year.component.html',
  styleUrls: [
    '../display.component.scss'
  ],
})
export class AccidentsLastYearComponent implements OnInit {
  @Input() accidents: number = 0;
  @Output() accidentsChange = new EventEmitter<number>();

  @Input() manuelModus: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  updateAccidents(event: any) {
    if (event !== undefined && event !== null) {
      if (event.target !== undefined && event.target !== null) {
        if (event.target.value !== undefined || event.target.value !== null) {
          if (event.target.value === "") {
            this.accidents = 0;
            this.accidentsChange.emit(0);
          } else {
            this.accidents = Number(event.target.value);
            this.accidentsChange.emit(this.accidents);
          }
        }
      }
    }
  }
}
