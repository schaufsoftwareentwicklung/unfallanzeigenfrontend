import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatFormFieldModule } from '@angular/material/form-field';

import { DisplayComponent } from './display.component';
import { GTXTextComponent } from './gtx-text/gtx-text.component';
import { DaysWithoutAccidentsComponent } from './days-without-accidents/days-without-accidents.component';
import { DaysWithoutAccidentsHighscoreComponent } from './days-without-accidents-highscore/days-without-accidents-highscore.component';
import { AccidentsLastYearComponent } from './accidents-last-year/accidents-last-year.component';
import { AccidentsThisYearComponent } from './accidents-this-year/accidents-this-year.component';

import { NumberOnlyDirective } from './directives/number-only.directive';



@NgModule({
  declarations: [
    DisplayComponent,
    GTXTextComponent,
    DaysWithoutAccidentsComponent,
    DaysWithoutAccidentsHighscoreComponent,
    AccidentsLastYearComponent,
    AccidentsThisYearComponent,
    NumberOnlyDirective,
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
  ],
  exports: [
    DisplayComponent,
    GTXTextComponent,
    DaysWithoutAccidentsComponent,
    DaysWithoutAccidentsHighscoreComponent,
    AccidentsLastYearComponent,
    AccidentsThisYearComponent,
    NumberOnlyDirective,
  ]
})
export class DisplayModule { }
