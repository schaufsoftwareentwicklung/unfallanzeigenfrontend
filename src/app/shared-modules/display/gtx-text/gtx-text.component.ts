import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-gtx-text',
  templateUrl: './gtx-text.component.html',
  styleUrls: [
    '../display.component.scss'
  ]
})
export class GTXTextComponent implements OnInit {
  @Input("row1") row1: string = "Achtung!";
  @Input("row2") row2: string = "Bitte passen Sie auf.";
  @Input() rowsCount: number = 1;
  @Input() characterCount: number = 16;
  
  @Output() row1Change = new EventEmitter<string>();
  @Output() row2Change = new EventEmitter<string>();
  
  constructor() { }

  ngOnInit(): void {
  }

  updateTextRow(event: any, row: number) {
    if (event !== undefined && event !== null) {
      if (event.target !== undefined && event.target !== null) {
        if (event.target.value !== undefined || event.target.value !== null) {
          if (row === 1) {
            this.row1 = event.target.value;
            this.row1Change.emit(this.row1);
          } else {
            this.row2 = event.target.value;
            this.row2Change.emit(this.row2);
          }
        }
      }
    }
  }

}
