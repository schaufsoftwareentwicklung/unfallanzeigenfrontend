import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigurationModule } from './configuration/configuration.module';
import { MonitorModule } from './monitor/monitor.module';
import { WorkingModule } from './working/working.module';

const routes: Routes = [
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    MonitorModule,
    WorkingModule,
    ConfigurationModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
