import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {
  PageTitle: String = "Unfallanzeigensteuerung";

  constructor() { }

  ngOnInit(): void {
  }


  setPageTitle(title: String){
    this.PageTitle = title;
  }
}

