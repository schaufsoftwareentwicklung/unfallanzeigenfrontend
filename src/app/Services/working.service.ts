import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { IDisplay } from '../Models/Display.Model';
import { AppConfigService } from './app-config.service';

@Injectable({
  providedIn: 'root'
})

export class WorkingService {
  selectedDisplaySubject = new Subject<number>();
  selectedDisplayIndex: number = -1;
  displaysChanged = new Subject<IDisplay[]>();
  displayCounter: number = 0;
  LogSubject = new Subject<string>();
  webAPIServer: string;

  private displays: IDisplay[];


  constructor(private http: HttpClient, private appConfig: AppConfigService) {
    this.webAPIServer = appConfig.webAPIServer;
  }

  initialize() {
    this.http.get<IDisplay[]>(this.webAPIServer + "/api/display/get-all").subscribe(data => {
      this.displays = [...((data as any).displays as IDisplay[])];
    });
    this.selectedDisplaySubject.subscribe(index => this.selectedDisplayIndex = index);
  }

  triggerDisplaysChanged() {
    this.displaysChanged.next(this.displays);
  }

  interfaceSelected(selectedInterfaceIndex: number) {
    this.http.get<any>(this.webAPIServer + "/api/display/get-accident-data/" + this.displays[this.selectedDisplayIndex]._id + "/" + selectedInterfaceIndex).subscribe(res => {
      if (res.status === 201) {
        this.displays[this.selectedDisplayIndex] = res.display;
        this.displaysChanged.next(this.displays);
      } else {
        this.updateLog((res as any).message);
      }
    }, err => {
      console.log(err);
      this.updateLog((err as any).message);

    })
  }

  updateLog(log: string) {
    if (log !== null || log !== undefined) {
      let logtext = "";
      logtext += log + "\n";
      logtext += "--------\n"
      this.LogSubject.next(logtext);
    }
  }

  reportAccident(display: IDisplay) {
    this.http.post<any>(this.webAPIServer + "/api/display/report-accident/", display).subscribe(res => {
      if (res.status === 201) {
        this.displays[this.selectedDisplayIndex] = ((res as any).display as IDisplay);
        this.displaysChanged.next(this.displays);
      } 
        this.updateLog((res as any).message);
      
    }, err => {
      this.updateLog((err as any).message);
    })
  }

  sendAccidentData(display: IDisplay) {
    this.http.post<any>(this.webAPIServer + "/api/display/send-accident-data/", display).subscribe(res => {
      if (res.status === 201) {
        this.updateLog((res as any).message);
      } else {
        this.updateLog((res as any).message);
      }
    }, err => {
      this.updateLog((err as any).message);
    })
  }

  sendDateTimeData(display: IDisplay) {
    this.http.post<any>(this.webAPIServer + "/api/display/send-date-time-data/", display).subscribe(res => {
      if (res.status === 201) {
        this.updateLog((res as any).message);
      } else {
        this.updateLog((res as any).message);
      }
    }, err => {
      this.updateLog((err as any).message);
    })
  }

}
