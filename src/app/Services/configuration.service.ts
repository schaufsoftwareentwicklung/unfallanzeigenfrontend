import { AnimationDriver } from '@angular/animations/browser';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { IDisplay } from 'src/app/Models/Display.Model';
import { DisplayContent } from 'src/app/Models/DisplayContent.Model';
import { DisplayInterfaces } from 'src/app/Models/DisplayInterfaces.Enum';
import { AppConfigService } from './app-config.service';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {
  startedEditing = new Subject<number>();
  displaysChanged = new Subject<IDisplay[]>();
  displayCounter: number = 0;
  webAPIServer: string;

  private displays: IDisplay[];

  constructor(private http: HttpClient, private appConfig: AppConfigService) {
    this.webAPIServer = appConfig.webAPIServer;
  }


  initialize() {
    this.http.get<IDisplay[]>(this.webAPIServer + "/api/display/get-all").subscribe(data => {
      this.displays = [...((data as any).displays as IDisplay[])];
    });
  }


  triggerDisplaysChanged() {
    this.displaysChanged.next(this.displays);
  }

  duplicateDisplay(index: number) {
    let display = this.displays[index];
    display._id = "";
    this.displays.push(display);
    this.http.post<IDisplay>(this.webAPIServer + "/api/display/add/", display).subscribe(res => {
      display._id = res._id;
      console.log((res as any).message);
    })

    this.displaysChanged.next(this.displays);

  }

  getDisplay(index: number): IDisplay {
    return this.displays[index];
  }

  newDisplay() {
    this.displays.push(this.createEmptyDisplay());
    this.displaysChanged.next(this.displays.slice());
  }

  saveDisplay(index: number, newDisplay: IDisplay) {
    this.displays[index] = newDisplay;
    if (newDisplay._id == "") {

      this.http.post<IDisplay>(this.webAPIServer + "/api/display/add/", newDisplay).subscribe(res => {
        newDisplay._id = res._id;
        console.log((res as any).message);
      })
    }
    else {
      this.http.patch<IDisplay>(this.webAPIServer + "/api/display/update/" + newDisplay._id, newDisplay).subscribe(res => {
        console.log((res as any).message);
      });
    }
    this.displaysChanged.next(this.displays);
  }

  deleteDisplay(index: number) {
    this.http.delete<IDisplay>(this.webAPIServer + "/api/display/delete/" + this.displays[index]._id).subscribe(res => {
      console.log((res as any).message);
    });
    this.displays.splice(index, 1);
    this.displaysChanged.next(this.displays.slice());
  }


  createEmptyDisplay() {
    var newDisplay: IDisplay = new IDisplay();

    newDisplay._id = ""

    newDisplay.name = "Neues Display " + (this.displays.length + 1);

    newDisplay.useAccidentsLastYear = false;
    newDisplay.useAccidentsThisYear = false;
    newDisplay.useDaysWithoutAccidents = false;
    newDisplay.useDaysWithoutAccidentsHighscore = false;
    newDisplay.useGtxText = false;
    newDisplay.gtxRows = 1;
    newDisplay.gtxTextLength = 16;
    newDisplay.displayInterface = DisplayInterfaces.Network;
    newDisplay.interfaces = ['192.168.0.0:10001'];
    newDisplay.manualModus = false;
    newDisplay.displayContent = new DisplayContent();

    return newDisplay;
  }
}
