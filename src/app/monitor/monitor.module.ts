import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonitorComponent } from './monitor.component';
import { DisplayModule } from '../shared-modules/display/display.module';
import { RouterModule, Routes } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

const routes: Routes = [
  { path: 'Monitor', component: MonitorComponent}
];

@NgModule({
  declarations: [MonitorComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    DisplayModule,
    MatListModule,
    MatButtonModule,
    MatIconModule
  ]
})
export class MonitorModule { }
