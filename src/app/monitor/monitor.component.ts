import { Component, OnDestroy, OnInit } from '@angular/core';
import { io } from 'socket.io-client';
import { IDisplay } from '../Models/Display.Model';
import { AppConfigService } from '../Services/app-config.service';

@Component({
  selector: 'app-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: [
    './monitor.component.scss',
    '../styles/global/list.scss',
    '../styles/global/layout.scss',
    '../styles/global/box.scss'
  ]
})
export class MonitorComponent implements OnInit, OnDestroy {
  private socket;
  public displays: IDisplay[];
  public selectedDisplay: IDisplay = new IDisplay();
  public selectedDisplayID: string = "0";
  public hideDisplaySelect: boolean = false;
  public displayColClass: String = "col-9"

  constructor(private appConfig: AppConfigService) { }

  ngOnInit(): void {
    this.setupSocketConnection();
  }


  setupSocketConnection() {
    this.socket = io(this.appConfig.webAPIServer, {
      auth: {
        token: 'BSf@s=hP>W[aP$HP:5E*,m*e$1p3e5Nio{9fcT]A$f4BJ<KW,@9Y{n4jl`C9r8)|S1>I!z8a49:>OJ9VqmD%tOCC/r*3[;``CH{]'
      }
    });

    this.socket.emit("getDisplays");

    this.socket.on("sendDisplays", (newDisplays) => {
      this.displays = newDisplays;
      console.log(this.displays);
      if (this.displays.some(e => e._id === this.selectedDisplayID)){
        this.selectedDisplay = this.displays.find(x => x._id === this.selectedDisplayID)!;
      } else {
        if (this.displays.length > 1){
          this.selectedDisplay = this.displays[0];
          this.selectedDisplayID = this.selectedDisplay._id;
        }
      }
    });

    this.socket.on("displayChanged", (display) => {
      console.log(display);
      this.displays[this.displays.findIndex(x => x._id == display._id)] = display;

      if (this.selectedDisplayID == display._id){
        this.selectedDisplay = display;
      }
    });

    this.socket.on("displayAdded", (display) => {
      this.displays.push(display);
    });

    this.socket.on("displayDeleted", (id) => {
      this.displays.splice(this.displays.findIndex(x => x._id == id), 1);
    })


  }

  onSelectDisplay(index: number) {
    this.selectedDisplay = this.displays[index];
    this.selectedDisplayID = this.selectedDisplay._id;
  }

  hideDisplaySelectClicked() {
    if(this.hideDisplaySelect){
      this.hideDisplaySelect = false;
      this.displayColClass = "col-9";
    }else {
      this.hideDisplaySelect = true;
      this.displayColClass = "col-10";
    }
  }

  reload() {
    this.socket.emit("getDisplays");
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.socket.disconnect();
  }
}
