import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';

import { DisplayModule } from '../shared-modules/display/display.module';

import { WorkingFacadeComponent } from './working-facade.component';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  { path: '', redirectTo:'/Anzeigen', pathMatch: 'full'}, 
  { path: 'Anzeigen', component: WorkingFacadeComponent}, 
];

@NgModule({
  declarations: [
    WorkingFacadeComponent,
  ],
  imports: [
    CommonModule,
    DisplayModule,
    MatListModule,
    MatIconModule,
    MatFormFieldModule,
    MatButtonModule,
    RouterModule.forChild(routes)
  ]
})
export class WorkingModule { }
