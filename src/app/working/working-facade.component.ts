import { formatDate } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { IDisplay } from 'src/app/Models/Display.Model';
import { WorkingService } from 'src/app/Services/working.service';
import * as moment from 'moment';

@Component({
  selector: 'app-working-facade',
  templateUrl: './working-facade.component.html',
  styleUrls: [
    './working-facade.component.scss',
    '../styles/global/list.scss',
    '../styles/global/layout.scss',
    '../styles/global/box.scss'
  ]
})
export class WorkingFacadeComponent implements OnInit, OnDestroy {
  selectedDisplay: IDisplay = new IDisplay();
  private subscriptions: Subscription[] = [];
  displays: IDisplay[] = [];
  selectedDisplayIndex: number = -1;
  selectedInterfaceIndex: number = -1;
  logText: string = "";
  isTimeValid: boolean = false;
  isDateValid: boolean = false;


  constructor(private workingService: WorkingService) { }

  ngOnInit(): void {
    this.workingService.initialize();

    setTimeout(() => {
      this.subscriptions.push(this.workingService.displaysChanged.subscribe(displays => {
        this.displays = [...displays];
        if (this.selectedDisplayIndex !== -1) {
          this.selectedDisplay = this.displays[this.selectedDisplayIndex];
          this.checkDateAndTimeManually();
        }
      }));

      this.subscriptions.push(this.workingService.LogSubject.subscribe(log => {
        this.logText += log;
      }))


      setTimeout(() => {
        this.workingService.triggerDisplaysChanged();

        setTimeout(() => {
          if (this.displays.length != 0) {
            {
              this.onSelectDisplay(0);
              if (this.displays[this.selectedDisplayIndex].interfaces.length != 0) {
                this.onSelectInterface(0);
              }
            }
          }
        }, 500);
      }, 500);
    }, 500);


    
  }

  onSelectDisplay(index: number) {
    this.workingService.selectedDisplaySubject.next(index);
    this.selectedDisplay = this.displays[index];
    this.selectedDisplayIndex = index;
    this.checkDateAndTimeManually();
  }

  onSelectInterface(index: number) {
    this.selectedInterfaceIndex = index;
    if (this.selectedDisplay.useAccidentsLastYear || this.selectedDisplay.useAccidentsThisYear ||
      this.selectedDisplay.useDaysWithoutAccidents || this.selectedDisplay.useDaysWithoutAccidentsHighscore) {
      this.workingService.interfaceSelected(this.selectedInterfaceIndex);
      this.checkDateAndTimeManually();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    })
  }

  onDisplayChanged(event: IDisplay) {
    this.selectedDisplay = event;
  }

  setTime(event: any) {
    if (event !== undefined && event !== null) {
      if (event.target !== undefined && event.target !== null) {
        if (event.target.value !== undefined || event.target.value !== null) {
          if (event.target.value.match(/^([0-2][0-3]:[0-5][0-9]|[0-1][0-9]:[0-5][0-9])$/g)) {
            this.isTimeValid = true;
            this.selectedDisplay.time = event.target.value;
          } else {
            this.isTimeValid = false;
          }
        }
      }
    }
  }

  checkDateAndTimeManually() {
    if (this.selectedDisplay?.time?.match(/^([0-2][0-3]:[0-5][0-9]|[0-1][0-9]:[0-5][0-9])$/g)) {
      this.isTimeValid = true;
    } else {
      this.isTimeValid = false;
    }

    if (moment(this.selectedDisplay.date, "DD.MM.YY", true).isValid()) {
      this.isDateValid = true;
    } else {
      this.isDateValid = false;
    }
  }

  setDate(event: any) {
    if (event !== undefined && event !== null) {
      if (event.target !== undefined && event.target !== null) {
        if (event.target.value !== undefined || event.target.value !== null) {
          if (moment(event.target.value, "DD.MM.YY", true).isValid()) {
            this.selectedDisplay.date = event.target.value;
            this.isDateValid = true;
          } else {
            this.isDateValid = false;
          }
        }
      }
    }
  }

  reportAccident() {
    this.workingService.reportAccident(this.selectedDisplay);
  }

  sendData() {
    this.workingService.sendAccidentData(this.selectedDisplay);
  }

  sendSystemTime() {
    let dateTimeNow = new Date();
    this.selectedDisplay.date = formatDate(dateTimeNow, 'dd.MM.yy', 'de').toString();
    this.selectedDisplay.time = formatDate(dateTimeNow, 'HH:mm', 'de').toString();
    this.saveTime();
  }

  saveTime() {
    this.workingService.sendDateTimeData(this.selectedDisplay);
  }

  clearLog() {
    this.logText = "";
  }
}
