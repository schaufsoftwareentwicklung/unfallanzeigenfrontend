import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { HeaderComponent } from './header/header.component';
import { WorkingService } from './Services/working.service';
import { ConfigurationService } from './Services/configuration.service';

import { DisplayModule } from './shared-modules/display/display.module';
import { WorkingModule } from './working/working.module';
import { ConfigurationModule } from './configuration/configuration.module';
import { MonitorModule } from './monitor/monitor.module';
import { LayoutDesignerModule } from './layout-designer/layout-designer.module';

import { AppConfigService } from './Services/app-config.service';

import localeDe from '@angular/common/locales/de';
import { registerLocaleData } from '@angular/common';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu'; 
import { MatIconModule } from '@angular/material/icon';



registerLocaleData(localeDe);

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    NgxDatatableModule,
    BrowserAnimationsModule,
    HttpClientModule,
    WorkingModule,
    DisplayModule,
    ConfigurationModule,
    MonitorModule,
    LayoutDesignerModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule
  ],
  providers: [
    ConfigurationService,
    WorkingService,
    AppConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: (appConfigService: AppConfigService) => () => appConfigService.loadAppConfig(),
      multi: true,
      deps: [AppConfigService]
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {


}
