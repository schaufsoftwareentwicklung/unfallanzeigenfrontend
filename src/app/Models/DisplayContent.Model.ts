export class DisplayContent {

        constructor(
                public accidentsLastYear: number = 0,
                public accidentsThisYear: number = 0,
                public daysWithoutAccidents: number = 0,
                public daysWithoutAccidentsHighscore: number = 0,

                public gtxTextFirstRow: string = "",
                public gtxTextSecoundRow: string = ""
                ) { }
}