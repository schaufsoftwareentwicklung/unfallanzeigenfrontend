import { DisplayContent } from "./DisplayContent.Model";
import { DisplayInterfaces } from "./DisplayInterfaces.Enum";

export class IDisplay {

        public _id: string;
        public name: string;
        
        public useAccidentsLastYear: boolean;
        public useAccidentsThisYear: boolean;
        public useDaysWithoutAccidents: boolean;
        public useDaysWithoutAccidentsHighscore: boolean;
        
        public useGtxText: boolean;
        public gtxRows: number;
        public gtxTextLength: number;
        
        public displayInterface: DisplayInterfaces;
        
        // public interfaces: { adress: string; }[];
        public interfaces: string[];
        
        public manualModus: boolean;
        public time: string;
        public date: string;

        public displayContent: DisplayContent;

        constructor() {
                this._id="",
                this.name="",
                this.useAccidentsLastYear=false,
                this.useAccidentsThisYear=false,
                this.useDaysWithoutAccidents=false,
                this.useDaysWithoutAccidentsHighscore=false,
                this.useGtxText=false,
                this.gtxRows=2,
                this.gtxTextLength=16,
                this.displayInterface= DisplayInterfaces.Network,
                this.interfaces=[],
                this.manualModus=false,
                this.time= "00:00",
                this.date= "01.01.01",
                this.displayContent= new DisplayContent()
        }
}
