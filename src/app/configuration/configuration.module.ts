import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatRadioModule } from '@angular/material/radio';
import { MatToolbarModule } from '@angular/material/toolbar';

import { DisplayModule } from '../shared-modules/display/display.module';

import { ConfigurationFacadeComponent } from './configuration-facade.component';
import { DisplayConfigurationFormComponent } from './display-configuration-form/display-configuration-form.component';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  { path: 'Konfiguration', component: ConfigurationFacadeComponent }
];


@NgModule({
  declarations: [
    ConfigurationFacadeComponent,
    DisplayConfigurationFormComponent
  ],
  imports: [
    CommonModule,
    DisplayModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatListModule,
    MatButtonModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class ConfigurationModule { }
