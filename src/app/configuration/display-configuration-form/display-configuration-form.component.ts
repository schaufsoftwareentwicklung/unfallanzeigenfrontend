import { Output } from '@angular/core';
import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_CHECKBOX_DEFAULT_OPTIONS } from '@angular/material/checkbox';
import { MAT_RADIO_DEFAULT_OPTIONS } from '@angular/material/radio';
import { Subscription } from 'rxjs';
import { IDisplay } from 'src/app/Models/Display.Model';
import { DisplayInterfaces } from 'src/app/Models/DisplayInterfaces.Enum';
import { ConfigurationService } from 'src/app/Services/configuration.service';


@Component({
  selector: 'app-display-configuration-form',
  templateUrl: './display-configuration-form.component.html',
  styleUrls: [
    './display-configuration-form.component.scss',
    '../configuration-facade.component.scss',
    '../../styles/global/list.scss',
    '../../styles/global/layout.scss',
    '../../styles/global/box.scss'
  ],
  providers: [
    {
      provide: MAT_RADIO_DEFAULT_OPTIONS,
      useValue: { color: 'primary' },
  },
  {
    provide: MAT_CHECKBOX_DEFAULT_OPTIONS,
    useValue: { color: 'primary' },
}
  ]
})
export class DisplayConfigurationFormComponent implements OnInit, OnDestroy {
  //Regex to validate "<ip>:<Port>""
  IpPortRegex: string = '^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]):(0|[1-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$';
  //Regex to Validate "COM<number>"
  ComPortRegex: string = '[C|c][O|o][M|m][0-9]+$';
  //Regex to Validate 1 oder 2
  GtxRowsRegex: string = '^((1)|(2))$';
  //Regex to Validate 16 oder 24
  GtxTextLengthRegex: string = '^((16)|(24))$';

  private subscription: Subscription = new Subscription;

  public display: IDisplay = new IDisplay();
  selectedDisplayIndex: number = -1;
  selectedInterfaceIndex: number = -1;
  public form: FormGroup;
  @Output() displayConfig = new EventEmitter<IDisplay>();

  constructor(
    private formBuilder: FormBuilder,
    private configurationService: ConfigurationService
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id: [{ value: '' }],
      name: [{ value: '' }, Validators.required],

      useAccidentsThisYear: [{ value: false }],
      useAccidentsLastYear: [{ value: false }],
      useDaysWithoutAccidents: [{ value: false }],
      useDaysWithoutAccidentsHighscore: [{ value: false }],

      useGtxText: [{ value: false }],
      gtxRows: [{ value: 1, disabled: !this.display.useGtxText }, Validators.pattern(this.GtxRowsRegex)],
      gtxTextLength: [{ value: 16, disabled: !this.display.useGtxText }, Validators.pattern(this.GtxTextLengthRegex)],


      displayInterface: [{ value: DisplayInterfaces.Network }, Validators.required],
      // interfaces: [{value: new FormArray([]), disabled: !this.canEdit}],
      interfaces: new FormArray([]),

      manualModus: [{ value: true }],
    })
    this.getData();
  }

  private getData(): void {

    setTimeout(() => {
      this.subscription = this.configurationService.startedEditing
        .subscribe(
          (index: number) => {

            this.selectedDisplayIndex = index;
            this.display = this.configurationService.getDisplay(index);
            this.form.patchValue(this.display);
            
            this.onUseGtxTextChanged();

            while ((this.form.controls.interfaces as FormArray).length !== 0) {
              (this.form.controls.interfaces as FormArray).removeAt(0)
            }
            this.display.interfaces.forEach(element => {
              (this.form.controls.interfaces as FormArray).push(new FormControl(element));
            })
            this.setInterfaceValidators();
          }
        );
    }, 500);

    setTimeout(() => {
      this.form.valueChanges.subscribe(display => {
        let data = { ...this.display, ...this.form.value };
        this.displayConfig.emit(data);
      });
    }, 500);
  }

  public notOnlyWhitespaceValidator(control: AbstractControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  get interfaces(): FormArray {
    return this.form.get('interfaces') as FormArray;
  }


  onUseGtxTextChanged() {
    if (this.form.value.useGtxText) {
      this.form.controls.gtxRows.setValidators([Validators.pattern(this.GtxRowsRegex), Validators.required]);
      this.form.controls['gtxRows'].enable();
      this.form.controls.gtxTextLength.setValidators([Validators.pattern(this.GtxTextLengthRegex), Validators.required]);
      this.form.controls['gtxTextLength'].enable();
    } else {
      this.form.controls.gtxRows.clearValidators();
      this.form.controls['gtxRows'].disable();
      this.form.controls.gtxTextLength.clearValidators();
      this.form.controls['gtxTextLength'].disable();
    }
  }

  getInterfaceControls(): AbstractControl[] {
    return (this.form.controls.interfaces as FormArray).controls;
  }

  addInterface() {
    if (this.form.value.displayInterface == DisplayInterfaces.Network) {
      (this.form.controls.interfaces as FormArray).push(new FormControl('', [Validators.pattern(this.IpPortRegex), this.notOnlyWhitespaceValidator]));
    }
    else if (this.form.value.displayInterface == DisplayInterfaces.RS232) {
      (this.form.controls.interfaces as FormArray).push(new FormControl('', [Validators.pattern(this.ComPortRegex), this.notOnlyWhitespaceValidator]));
    }
  }

  onInterfaceSelected(i: number) {
    this.selectedInterfaceIndex = i;
  }

  deleteSelectedInterface() {
    (this.form.controls.interfaces as FormArray).removeAt(this.selectedInterfaceIndex);
    this.selectedInterfaceIndex = -1;
  }

  setInterfaceValidators() {
    if (this.form.value.displayInterface == DisplayInterfaces.Network) {
      this.interfaces.controls.forEach(inter => {
        inter.setValidators([Validators.pattern(this.IpPortRegex), this.notOnlyWhitespaceValidator]);
        inter.updateValueAndValidity();
      });
    }
    else if (this.form.value.displayInterface == DisplayInterfaces.RS232) {
      this.interfaces.controls.forEach(inter => {
        inter.setValidators([Validators.pattern(this.ComPortRegex), this.notOnlyWhitespaceValidator])
        inter.updateValueAndValidity();
      });
    }
  }



  onReset() {
    this.configurationService.startedEditing.next(this.selectedDisplayIndex);
  }

  onSubmit() {
    let data = { ...this.display, ...this.form.value };
    this.configurationService.saveDisplay(this.selectedDisplayIndex, data);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
