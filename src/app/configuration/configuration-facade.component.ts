import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { IDisplay } from 'src/app/Models/Display.Model';
import { ConfigurationService } from 'src/app/Services/configuration.service';


@Component({
  selector: 'app-configuration-facade',
  templateUrl: './configuration-facade.component.html',
  styleUrls: [
    './configuration-facade.component.scss',
    '../styles/global/list.scss',
    '../styles/global/layout.scss',
    '../styles/global/box.scss'
  ]
})
export class ConfigurationFacadeComponent implements OnInit, OnDestroy {
  displays: IDisplay[] = [];
  selectedDisplayIndex: number = -1;
  selectedDisplay: IDisplay;
  private subscription: Subscription;
  currentDisplayConfig: IDisplay = new IDisplay();

  constructor(private configurationService: ConfigurationService) {
  }

  ngOnInit() {
    this.configurationService.initialize();

    setTimeout(() => {
      this.subscription = this.configurationService.displaysChanged.subscribe(displays => {
        this.displays = [...displays];
      });
      setTimeout(() => {
        this.configurationService.triggerDisplaysChanged();

        setTimeout(() => {
          if (this.displays.length == 0) {
            this.newDisplay();
          }
          this.onEditItem(0);
        }, 500);
      }, 500);
    }, 500);
  }

  configChanged(displayConfig: IDisplay) {
    this.currentDisplayConfig = displayConfig;
  }

  newDisplay() {
    this.configurationService.newDisplay();
    // this.onEditItem(this.displays.length -1);
  }

  deleteSelectedDisplay() {
    this.configurationService.deleteDisplay(this.selectedDisplayIndex);
    this.selectedDisplayIndex = -1;
  }

  duplicateSelectedDisplay() {
    this.configurationService.duplicateDisplay(this.selectedDisplayIndex);
  }


  onEditItem(index: number) {
    this.configurationService.startedEditing.next(index);
    this.selectedDisplayIndex = index;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
